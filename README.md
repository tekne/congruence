# congruence

An implementation of congruence closure, `CongruenceClosure`, which drives modifications to an arbitrary union-find ADT implementing the `UnionFind` trait. A trait is provided for implementing custom languages (`Language`) and equations over these languages (`Equation`) to perform congruence over, as well as for comparing (`EqMod`) and hashing (`HashMod`) terms modulo some equivalence relation, optionally passing along a context (e.g., a map from terms to disjoint-set forest indices.)

Based on Nieuwenhuis and Oliveras [Proof-producing Congruence Closure](https://www.cs.upc.edu/~oliveras/rta05.pdf), except without the proof production. Designed with memory usage and performance in mind.

A minimal [disjoint set forest](https://en.wikipedia.org/wiki/Disjoint-set_data_structure) implementation with dense `usize` nodes, `DisjointSetForest`, is also provided.
