/*!
The `UnionFind` trait and associated utilities and implementations
*/

/// A trait implemented by data-structures implementing the union-find ADT.
///
/// See [`DisjointSetForest`] for an example implementation and usage
pub trait UnionFind<I = usize> {
    /// Take the union of two nodes, returning the new root.
    ///
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// # let mut dsf = DisjointSetForest::with_capacity(3);
    /// # let a = 0;
    /// # let b = 1;
    /// let union = dsf.union_find(a, b);
    /// ```
    /// is equivalent to
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// # let mut dsf = DisjointSetForest::with_capacity(3);
    /// # let a = 0;
    /// # let b = 1;
    /// dsf.union(a, b);
    /// let union = dsf.find(a);
    /// assert_eq!(union, dsf.find(b));
    /// ```
    /// but may be optimized better.
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::with_capacity(3);
    /// assert!(!dsf.node_eq(0, 1));
    /// assert!(!dsf.node_eq(0, 2));
    /// assert!(!dsf.node_eq(1, 2));
    /// let union = dsf.union_find(0, 1);
    /// assert!(union == 0 || union == 1);
    /// assert!(dsf.node_eq(0, 1));
    /// assert!(!dsf.node_eq(0, 2));
    /// assert!(!dsf.node_eq(1, 2));
    /// ```
    fn union_find(&mut self, left: I, right: I) -> I;

    /// Get what *would* be the new root if two nodes were unioned.
    ///
    /// That is, the following behaviour is guaranteed
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// # let mut dsf = DisjointSetForest::with_capacity(3);
    /// # let a = 0;
    /// # let b = 1;
    /// # let c = 2;
    /// # let z = 3;
    /// let union = dsf.pre_union_find(a, b);
    /// // Note that, at this point, it is *not* guaranteed that `dsf.node_eq(a, b)`!
    /// dsf.find(c);
    /// // ...
    /// dsf.find(z);
    /// assert_eq!(dsf.union_find(a, b), union);
    /// ```
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// # let mut dsf = DisjointSetForest::with_capacity(3);
    /// dsf.union(0, 1);
    /// let union = dsf.find(0);
    /// assert_eq!(union, dsf.find(1));
    /// ```
    /// but may be optimized better.
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::with_capacity(3);
    /// assert!(!dsf.node_eq(0, 1));
    /// assert!(!dsf.node_eq(0, 2));
    /// assert!(!dsf.node_eq(1, 2));
    /// let union = dsf.union_find(0, 1);
    /// assert!(union == 0 || union == 1);
    /// assert!(dsf.node_eq(0, 1));
    /// assert!(!dsf.node_eq(0, 2));
    /// assert!(!dsf.node_eq(1, 2));
    /// ```
    fn pre_union_find(&mut self, left: I, right: I) -> I;

    /// Merge the equivalence classes of two nodes
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::with_capacity(3);
    /// assert!(!dsf.node_eq(0, 1));
    /// assert!(!dsf.node_eq(0, 2));
    /// assert!(!dsf.node_eq(1, 2));
    /// dsf.union(0, 1);
    /// assert!(dsf.node_eq(0, 1));
    /// assert!(!dsf.node_eq(0, 2));
    /// assert!(!dsf.node_eq(1, 2));
    /// ```
    fn union(&mut self, left: I, right: I);

    /// Find the representative of a node, without requiring a mutable borrow of this data structure.
    ///
    /// Always returns the same result as [`Self::find`], but may not optimize the underlying data structure.
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::with_capacity(3);
    /// assert_eq!(dsf.find(0), 0);
    /// assert_eq!(dsf.find(1), 1);
    /// dsf.union(0, 1);
    /// assert_eq!(dsf.find(0), dsf.find(1));
    /// ```
    fn find(&self, node: I) -> I;

    /// Check whether two nodes are in the same equivalence class, without requiring a mutable borrow of this data structure.
    ///
    /// Always returns the same result as [`Self::node_eq_mut`], but may not optimize the underlying data structure.
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::with_capacity(10);
    /// dsf.union(3, 5);
    /// dsf.union(6, 3);
    /// dsf.union(7, 2);
    /// for i in 0..10 {
    ///     for j in 0..10 {
    ///         assert_eq!(dsf.node_eq_mut(i, j), dsf.node_eq(i, j))
    ///     }
    /// }
    /// ```
    fn node_eq(&self, left: I, right: I) -> bool;

    /// Find the representative of a node, while potentially optimizing the underlying data structure.
    ///
    /// Always returns the same result as [`Self::find`], but may optimize the underlying data structure.
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::with_capacity(3);
    /// assert_eq!(dsf.find_mut(0), 0);
    /// assert_eq!(dsf.find_mut(1), 1);
    /// dsf.union(0, 1);
    /// assert_eq!(dsf.find_mut(0), dsf.find_mut(1));
    /// ```
    #[inline(always)]
    fn find_mut(&mut self, node: I) -> I {
        self.find(node)
    }

    /// Check whether two nodes are in the same equivalence class, without requiring a mutable borrow of this data structure.
    ///
    /// Always returns the same result as [`Self::node_eq`], but may optimize the underlying data structure.
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::with_capacity(10);
    /// dsf.union(3, 5);
    /// dsf.union(6, 3);
    /// dsf.union(7, 2);
    /// for i in 0..10 {
    ///     for j in 0..10 {
    ///         assert_eq!(dsf.node_eq_mut(i, j), dsf.node_eq(i, j))
    ///     }
    /// }
    /// ```
    #[inline(always)]
    fn node_eq_mut(&mut self, left: I, right: I) -> bool {
        self.node_eq(left, right)
    }
}
