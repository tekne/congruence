/*!
An implementation of congruence closure, [`CongruenceClosure`], which drives modifications to an arbitrary union-find ADT implementing the [`UnionFind`] trait.
Based on Nieuwenhuis and Oliveras [Proof-producing Congruence Closure](https://www.cs.upc.edu/~oliveras/rta05.pdf), except without the proof production.

A minimal [disjoint set forest](https://en.wikipedia.org/wiki/Disjoint-set_data_structure) implementation with dense `usize` nodes, [`DisjointSetForest`], is also provided.
*/
#![forbid(missing_docs, missing_debug_implementations)]
use core::{
    cmp::Ordering,
    fmt::{self, Debug, Formatter},
    hash::{BuildHasher, Hash},
};
use hashbrown::{raw::RawTable, HashSet};
use smallvec::SmallVec;
use std::{collections::BTreeSet, convert::Infallible};

mod disjoint_set_forest;
mod language;
mod union_find;

pub use disjoint_set_forest::*;
pub use language::*;
pub use union_find::*;

/// An implementation of congruence closure, parametric over an arbitrary disjoint set forest implementation.
///
/// Based on Nieuwenhuis and Oliveras [Proof-producing Congruence Closure](https://www.cs.upc.edu/~oliveras/rta05.pdf), except without the proof production.
///
/// # Examples
/// ```rust
/// # use congruence::{DisjointSetForest, CongruenceClosure, UseSet, UnionFind, Pending, Equate};
/// let mut dsf = DisjointSetForest::with_capacity(5);
/// let mut cc = CongruenceClosure::new();
/// let mut us = UseSet::new();
/// let mut st = Pending::default();
/// let a = 0;
/// let b = 1;
/// let c = 2;
/// let d = 3;
/// let e = 4;
/// let f = 5;
/// // Set a * b = c
/// cc.equation(Equate((a, b), c), &mut dsf, &mut us, &mut st);
/// assert!(dsf.is_empty());
/// // Set d * e = f
/// cc.equation(Equate((d, e), f), &mut dsf, &mut us, &mut st);
/// assert!(dsf.is_empty());
/// // Set a = d
/// cc.merge(a, d, &mut dsf, &mut us, &mut st);
/// assert!(!dsf.is_empty());
/// for i in 0..5 {
///     for j in 0..5 {
///         assert_eq!(dsf.node_eq(i, j), i == j || (i == a || i == d) && (j == a || j == d))  
///     }
/// }
/// // Set b = e
/// cc.merge(b, e, &mut dsf, &mut us, &mut st);
/// assert!(dsf.node_eq(b, e));
/// // By congruence, we now have that c = a * b = d * e = f
/// assert!(dsf.node_eq(c, f));
/// assert!(dsf.node_eq(a, d));
/// assert!(!dsf.node_eq(a, b));
/// assert!(!dsf.node_eq(a, c));
/// assert!(!dsf.node_eq(a, e));
/// assert!(!dsf.node_eq(a, f));
/// assert!(!dsf.node_eq(b, c));
/// assert!(!dsf.node_eq(b, d));
/// assert!(!dsf.node_eq(b, f));
/// assert!(!dsf.node_eq(c, d));
/// assert!(!dsf.node_eq(c, e));
/// assert!(!dsf.node_eq(d, e));
/// assert!(!dsf.node_eq(d, f));
/// assert!(!dsf.node_eq(e, f));
/// ```
#[derive(Clone)]
pub struct CongruenceClosure<E, S = hashbrown::hash_map::DefaultHashBuilder> {
    /// A lookup table from terms in a language modulo some equivalence relation to representatives of that term
    lookup: RawTable<E>,
    /// The hasher for  `lookup`
    hasher: S,
}

impl<E: Debug, S> Debug for CongruenceClosure<E, S> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut set = f.debug_set();
        //SAFETY: lookup is not dropped or modified for the duration of this block
        unsafe {
            for lookup in self.lookup.iter() {
                set.entry(lookup.as_ref());
            }
        }
        set.finish()
    }
}

impl<E, S: Default + BuildHasher> Default for CongruenceClosure<E, S> {
    fn default() -> Self {
        Self {
            lookup: Default::default(),
            hasher: Default::default(),
        }
    }
}

impl<E> CongruenceClosure<E> {
    /// Create a new, empty congruence closure
    ///
    /// # Example
    /// ```rust
    /// # use congruence::{CongruenceClosure, Equate};
    /// let cc = CongruenceClosure::<Equate<(usize, usize), usize>>::new();
    /// assert!(cc.is_empty());
    /// ```
    #[inline]
    pub fn new() -> Self {
        CongruenceClosure {
            lookup: Default::default(),
            hasher: Default::default(),
        }
    }

    /// Create a new, empty congruence closure with the given node and pair capacities
    ///
    /// # Example
    /// ```rust
    /// # use congruence::{CongruenceClosure, Equate};
    /// let cc = CongruenceClosure::<Equate<(usize, usize), usize>>::with_capacity(5);
    /// assert!(cc.is_empty());
    /// ```
    #[inline]
    pub fn with_capacity(capacity: usize) -> Self {
        CongruenceClosure {
            lookup: RawTable::with_capacity(capacity),
            hasher: Default::default(),
        }
    }
}

impl<E, S> CongruenceClosure<E, S>
where
    S: BuildHasher,
{
    /// Create a new, empty congruence closure with the given hasher
    #[inline]
    pub fn with_hasher(hasher: S) -> Self
    where
        S: Clone,
    {
        CongruenceClosure {
            lookup: RawTable::new(),
            hasher,
        }
    }

    /// Create a new, empty congruence closure with the given capacity and hasher
    #[inline]
    pub fn with_capacity_and_hasher(capacity: usize, hasher: S) -> Self
    where
        S: Clone,
    {
        CongruenceClosure {
            lookup: RawTable::with_capacity(capacity),
            hasher,
        }
    }

    /// Whether this congruence closure is empty, i.e. contains no *congruence* relations
    ///
    /// # Example
    /// ```rust
    /// # use congruence::{CongruenceClosure, DisjointSetForest, Pending, Equate, UseSet};
    /// let mut cc = CongruenceClosure::<Equate<(usize, usize), usize>>::new();
    /// let mut dsf = DisjointSetForest::new();
    /// let mut us = UseSet::new();
    /// let mut cs = Pending::new();
    /// assert!(cc.is_empty());
    ///
    /// // This operation only adds something to the disjoint set forest, leaving the congruence
    /// // closure itself empty
    /// cc.merge(3, 4, &mut dsf, &mut us, &mut cs);
    /// assert!(cc.is_empty());
    /// assert!(!dsf.is_empty());
    ///
    /// // We now actually add something to the congruence closure by equating an *expression*, here
    /// // simply a pair of `usize` indices, to a term
    /// cc.equation(Equate((1, 2), 3), &mut dsf, &mut us, &mut cs);
    /// assert!(!cc.is_empty());
    /// assert!(!dsf.is_empty());
    ///
    /// // On the other hand, equating an expression to a term without introducing any new term-level
    /// // equalities only adds things to the congruence closure, leaving the union-find structure
    /// // unchanged:
    /// cc.clear();
    /// dsf.clear();
    /// cc.equation(Equate((1, 2), 3), &mut dsf, &mut us, &mut cs);
    /// assert!(!cc.is_empty());
    /// assert!(dsf.is_empty());
    /// ```
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.lookup.len() == 0
    }

    /// Clear this congruence closure, maintaining it's capacity but otherwise resetting it to empty
    ///
    /// # Example
    /// ```rust
    /// # use congruence::{CongruenceClosure, DisjointSetForest, Pending, Equate, UseSet};
    /// let mut cc = CongruenceClosure::new();
    /// let mut dsf = DisjointSetForest::new();
    /// let mut us = UseSet::new();
    /// let mut cs = Pending::new();
    /// assert!(cc.is_empty());
    /// cc.equation(Equate((1, 2), 3), &mut dsf, &mut us, &mut cs);
    /// assert!(!cc.is_empty());
    /// cc.clear();
    /// assert!(cc.is_empty());
    /// ```
    #[inline]
    pub fn clear(&mut self) {
        self.lookup.clear();
    }

    /// Get an equation `eqn` such that `eqn.eq_mod(expr, union_find) == true`, if one exists.
    ///
    /// # Example
    /// ```rust
    /// # use congruence::{CongruenceClosure, DisjointSetForest, Pending, Equate, UseSet};
    /// let mut cc = CongruenceClosure::new();
    /// let mut dsf = DisjointSetForest::new();
    /// let mut us = UseSet::new();
    /// let mut cs = Pending::new();
    /// assert_eq!(cc.lookup(&(1, 2), &dsf), None);
    /// assert_eq!(cc.lookup(&(0, 2), &dsf), None);
    /// assert_eq!(cc.lookup(&(0, 1), &dsf), None);
    /// cc.equation(Equate((1, 2), 3), &mut dsf, &mut us, &mut cs);
    /// assert_eq!(cc.lookup(&(1, 2), &dsf), Some(&Equate((1, 2), 3)));
    /// assert_eq!(cc.lookup(&(0, 2), &dsf), None);
    /// assert_eq!(cc.lookup(&(0, 1), &dsf), None);
    /// cc.merge(0, 1, &mut dsf, &mut us, &mut cs);
    /// let repr = *cc.lookup(&(1, 2), &dsf).unwrap();
    /// assert_eq!(cc.lookup(&(0, 2), &dsf), Some(&repr));
    /// assert!(repr == Equate((1, 2), 3) || repr == Equate((0, 2), 3));
    /// assert_eq!(cc.lookup(&(0, 1), &dsf), None);
    /// ```
    pub fn lookup<B, U, I>(&self, expr: &B, union_find: &U) -> Option<&E>
    where
        I: Copy + Hash,
        B: HashMod<U, I>,
        E: EqMod<U, I, B> + Equation<U, I>,
    {
        let hash = expr.hash_one_mod(
            &self.hasher,
            &mut |hs, uf, ix| E::find(ix, uf).hash(hs),
            union_find,
        );
        self.lookup.get(hash, |eqn| {
            eqn.eq_mod(expr, &mut |uf, i, j| E::node_eq(i, j, uf), union_find)
        })
    }

    /// Get an equation `eqn` such that `eqn.eq_mod(expr, union_find) == true`, if one exists.
    ///
    /// Always returns the same result as [`Self::lookup`], but may optimize the union-find data structure provided in the process.
    pub fn lookup_mut<B, U, I>(&self, expr: &B, union_find: &mut U) -> Option<&E>
    where
        I: Copy + Hash,
        B: HashMod<U, I>,
        E: EqMod<U, I, B> + Equation<U, I>,
    {
        let hash = expr.hash_one_mod_mut(
            &self.hasher,
            &mut |hs, uf, ix| E::find_mut(ix, uf).hash(hs),
            union_find,
        );
        self.lookup.get(hash, |eqn| {
            eqn.eq_mod_mut(expr, &mut |uf, i, j| E::node_eq_mut(i, j, uf), union_find)
        })
    }

    /// Return whether there is an equation `eqn` such that `eqn.eq_mod(expr, union_find) == true` and `E::node_eq_mut(eqn.ix(), Some(ix), union_find)`
    ///
    /// # Example
    /// ```rust
    /// # use congruence::{CongruenceClosure, DisjointSetForest, Pending, Equate, UseSet};
    /// let mut cc = CongruenceClosure::new();
    /// let mut dsf = DisjointSetForest::new();
    /// let mut us = UseSet::new();
    /// let mut cs = Pending::new();
    ///
    /// assert!(!cc.expr_cong(&(0, 1), 4, &dsf));
    /// assert!(!cc.expr_cong(&(2, 3), 4, &dsf));
    ///
    /// cc.merge(0, 2, &mut dsf, &mut us, &mut cs);
    /// cc.merge(1, 3, &mut dsf, &mut us, &mut cs);
    /// assert!(!cc.expr_cong(&(0, 1), 4, &dsf));
    /// assert!(!cc.expr_cong(&(2, 3), 4, &dsf));
    ///
    /// // If we set `(0, 1) ~ 4`, we can deduce `(2, 3) ~ 4`, even though we've never inserted the pair `(2, 3)`
    /// cc.equation(Equate((0, 1), 4), &mut dsf, &mut us, &mut cs);
    /// assert!(cc.expr_cong(&(0, 1), 4, &dsf));
    /// assert!(cc.expr_cong(&(2, 3), 4, &dsf));
    /// ```
    pub fn expr_cong<B, U, I>(&self, expr: &B, ix: I, union_find: &U) -> bool
    where
        I: Copy + Hash,
        B: HashMod<U, I>,
        E: EqMod<U, I, B> + Equation<U, I>,
    {
        if let Some(eqn) = self.lookup(expr, union_find) {
            if let Some(eqn_ix) = eqn.ix(union_find) {
                return E::node_eq(eqn_ix, ix, union_find);
            }
        }
        false
    }

    /// Check whether an expression in the equation language is congruent to a given index
    ///
    /// Always returns the same result as [`Self::expr_cong`], but may optimize the union-find data structure provided in the process.
    pub fn expr_cong_mut<B, U, I>(&self, expr: &B, ix: I, union_find: &mut U) -> bool
    where
        I: Copy + Hash,
        B: HashMod<U, I>,
        E: EqMod<U, I, B> + Equation<U, I>,
    {
        if let Some(eqn) = self.lookup_mut(expr, union_find) {
            if let Some(eqn_ix) = eqn.ix_mut(union_find) {
                return E::node_eq_mut(eqn_ix, ix, union_find);
            }
        }
        false
    }

    /// Register an equation of the form `expr = result`, where `expr` is an expression in this congruence closure's language
    ///
    /// # Example
    /// ```rust
    /// # use congruence::{CongruenceClosure, DisjointSetForest, Pending, UnionFind, Equate, UseSet};
    /// let mut cc = CongruenceClosure::new();
    /// let mut dsf = DisjointSetForest::new();
    /// let mut us = UseSet::new();
    /// let mut cs = Pending::new();
    /// // Say 0 ~ 1 and 2 ~ 3
    /// cc.merge(0, 1, &mut dsf, &mut us, &mut cs);
    /// cc.merge(2, 3, &mut dsf, &mut us, &mut cs);
    /// assert!(dsf.node_eq(0, 1));
    /// assert!(dsf.node_eq(2, 3));
    /// assert!(!dsf.node_eq(4, 5));
    ///
    /// // If we set (0, 2) ~ 4 and (1, 3) ~ 5, we can then deduce 4 ~ 5
    /// cc.equation(Equate((0, 2), 4), &mut dsf, &mut us, &mut cs);
    /// assert!(dsf.node_eq(0, 1));
    /// assert!(dsf.node_eq(2, 3));
    /// assert!(!dsf.node_eq(4, 5));
    /// cc.equation(Equate((1, 3), 5), &mut dsf, &mut us, &mut cs);
    /// assert!(dsf.node_eq(0, 1));
    /// assert!(dsf.node_eq(2, 3));
    /// assert!(dsf.node_eq(4, 5));
    /// ```
    pub fn equation<U, I>(
        &mut self,
        eqn: E,
        union_find: &mut U,
        use_set: &mut UseSet<I>,
        pending: &mut Pending<I>,
    ) where
        I: Copy + Hash + Ord,
        E: Equation<U, I>,
    {
        self.equation_pending(eqn, union_find, use_set, pending);
        self.do_pending(union_find, use_set, pending);
        assert_eq!(pending.len(), 0);
    }

    /// Merge the equivalence classes of two nodes
    ///
    /// # Example    
    /// ```rust
    /// # use congruence::{CongruenceClosure, DisjointSetForest, Pending, UnionFind, Equate, UseSet};
    /// let mut cc = CongruenceClosure::new();
    /// let mut dsf = DisjointSetForest::new();
    /// let mut raw_dsf = DisjointSetForest::new();
    /// let mut us = UseSet::new();
    /// let mut cs = Pending::new();
    ///
    /// // Calls to `cc.merge` without any equations added to the congruence context are equivalent to calls to `dsf.union`
    /// cc.merge(0, 1, &mut dsf, &mut us, &mut cs);
    /// raw_dsf.union(0, 1);
    /// assert_eq!(dsf, raw_dsf);
    ///
    /// cc.merge(2, 3, &mut dsf, &mut us, &mut cs);
    /// raw_dsf.union(2, 3);
    /// assert_eq!(dsf, raw_dsf);
    ///
    /// // Similarly, with only one equation in the context, they do not change the union-find ADT,
    /// // though the equation itself may be modified as the union-find is updated:
    /// assert_eq!(cc.lookup(&(4, 5), &dsf), None);
    /// cc.equation(Equate((4, 5), 6), &mut dsf, &mut us, &mut cs);
    /// assert_eq!(dsf, raw_dsf);
    /// assert_eq!(cc.lookup(&(4, 5), &dsf), Some(&Equate((4, 5), 6)));
    ///
    /// cc.merge(0, 4, &mut dsf, &mut us, &mut cs);
    /// raw_dsf.union(0, 4);
    /// assert_eq!(dsf, raw_dsf);
    ///
    /// // Once we have a set of equations which overlap, however, calling `merge` can trigger multiple `unions` in the DSF,
    /// // implementing congruence closure:
    /// cc.equation(Equate((4, 7), 8), &mut dsf, &mut us, &mut cs);
    /// assert_eq!(dsf, raw_dsf);
    ///
    /// cc.merge(7, 5, &mut dsf, &mut us, &mut cs);
    /// raw_dsf.union(7, 5);
    /// assert!(raw_dsf.refines(&dsf));
    /// assert!(!dsf.refines(&raw_dsf));
    /// // In particular:
    /// assert!(!raw_dsf.node_eq(6, 8));
    /// assert!(dsf.node_eq(6, 8));
    /// ```
    pub fn merge<U, I>(
        &mut self,
        mut a: I,
        mut b: I,
        union_find: &mut U,
        use_set: &mut UseSet<I>,
        pending: &mut Pending<I>,
    ) where
        I: Copy + Ord + Hash,
        E: Equation<U, I>,
    {
        loop {
            self.merge_pending(a, b, union_find, use_set, pending);

            if let Some(pending) = pending.pop() {
                a = pending.0;
                b = pending.1;
            } else {
                return;
            }
        }
    }

    /// Update the dependencies on a node
    ///
    /// Note that, if `node` is not a representative anymore, only dependencies not yet attached to the new representative will be updated!
    pub fn update<U, I>(
        &mut self,
        node: I,
        union_find: &mut U,
        use_set: &mut UseSet<I>,
        pending: &mut Pending<I>,
    ) where
        I: Copy + Ord + Hash,
        E: Equation<U, I>,
    {
        self.update_pending(node, union_find, use_set, pending);
        self.do_pending(union_find, use_set, pending);
        assert_eq!(pending.len(), 0);
    }

    /// Register an equation of the form `expr = result`, placing any additional merges necessary to complete this change into the `pending` list
    pub fn equation_pending<U, I>(
        &mut self,
        eqn: E,
        union_find: &mut U,
        use_set: &mut UseSet<I>,
        pending: &mut Pending<I>,
    ) where
        I: Copy + Hash + Ord,
        E: Equation<U, I>,
    {
        let hash = eqn.hash_one_mod_mut(
            &self.hasher,
            &mut |hs, uf, ix| E::find_mut(ix, uf).hash(hs),
            union_find,
        );
        if let Some(curr_eqn) = self.lookup.get_mut(hash, |e| {
            eqn.eq_mod_mut(&e, &mut |uf, i, j| E::node_eq_mut(i, j, uf), union_find)
        }) {
            if let Some((left, right)) = curr_eqn.merge_pending(eqn, union_find, pending) {
                self.merge_pending(left, right, union_find, use_set, pending)
            }
        } else {
            eqn.visit_deps_mut(
                &mut |uf, ix| -> Result<(), Infallible> {
                    use_set.insert(E::find_mut(ix, uf), hash);
                    Ok(())
                },
                union_find,
            )
            .expect("infallible");
            self.lookup.insert(hash, eqn, |e| {
                e.hash_one_mod(
                    &self.hasher,
                    &mut |hs, uf, ix| E::find(ix, uf).hash(hs),
                    union_find,
                )
            });
        }
    }

    /// Merge the equivalence classes of two nodes, placing any additional merges necessary to complete this change into the `pending` list
    pub fn merge_pending<U, I>(
        &mut self,
        a: I,
        b: I,
        union_find: &mut U,
        use_set: &mut UseSet<I>,
        pending: &mut Pending<I>,
    ) where
        I: Copy + Ord + Hash,
        E: Equation<U, I>,
    {
        let a_repr = E::find_mut(a, union_find);
        let b_repr = E::find_mut(b, union_find);
        if a_repr != b_repr {
            let (new_repr, update_new) = E::merge_ix(a_repr, b_repr, union_find, pending);
            debug_assert!(E::find_mut(a, union_find) == new_repr);
            debug_assert!(E::find_mut(b, union_find) == new_repr);
            let old_repr = if new_repr == a_repr {
                b_repr
            } else {
                debug_assert!(new_repr == b_repr);
                a_repr
            };
            debug_assert!(old_repr != new_repr);
            self.update_pending(old_repr, union_find, use_set, pending);
            if update_new {
                self.update_pending(old_repr, union_find, use_set, pending);
            }
        }
    }

    /// Update the dependencies on a node, placing any additional operations necessary to finish this update into the `Pending` operation queue.
    ///
    /// Note that, if `node` is not a representative anymore, only dependencies not yet attached to the new representative will be updated!
    pub fn update_pending<U, I>(
        &mut self,
        node: I,
        union_find: &mut U,
        use_set: &mut UseSet<I>,
        pending: &mut Pending<I>,
    ) where
        I: Copy + Ord + Hash,
        E: Equation<U, I>,
    {
        let mut insertions: SmallVec<[(u64, E); 16]> = SmallVec::new();
        let mut min = 0;
        while let Some(old_hash) = use_set.pop(node, min) {
            debug_assert!(old_hash >= min);
            debug_assert_eq!(insertions.len(), 0);

            while let Some(old_eqn) = self.lookup.remove_entry(old_hash, |_| true) {
                let updated_hash = old_eqn.hash_one_mod_mut(
                    &self.hasher,
                    &mut |hasher, dsf, i| E::find_mut(i, dsf).hash(hasher),
                    union_find,
                );
                // Update dependencies
                old_eqn
                    .visit_deps_mut(
                        &mut |dsf, i| -> Result<(), Infallible> {
                            let i = E::find_mut(i, dsf);
                            use_set.remove(i, old_hash);
                            use_set.insert(i, updated_hash);
                            Ok(())
                        },
                        union_find,
                    )
                    .expect("infallible");

                if let Some(eqn) = self.lookup.get_mut(updated_hash, |eqn| {
                    eqn.eq_mod_mut(
                        &old_eqn,
                        &mut |dsf, i, j| E::node_eq_mut(i, j, dsf),
                        union_find,
                    )
                }) {
                    // Update unions to take into account previous equations
                    if let Some((left, right)) = eqn.merge_pending(old_eqn, union_find, pending) {
                        pending.push(left, right);
                    }
                } else {
                    // Queue equation to add to lookup table
                    insertions.push((updated_hash, old_eqn));
                }
            }

            while let Some((updated_hash, eqn)) = insertions.pop() {
                self.lookup.insert(updated_hash, eqn, |eqn| {
                    eqn.hash_one_mod(
                        &self.hasher,
                        &mut |hasher, dsf, i| E::find(i, dsf).hash(hasher),
                        union_find,
                    )
                });
            }

            if old_hash == u64::MAX {
                return;
            }
            min = old_hash + 1;
        }
    }

    /// Perform all pending operations in a congruence state, leaving it empty
    pub fn do_pending<U, I>(
        &mut self,
        union_find: &mut U,
        use_set: &mut UseSet<I>,
        pending: &mut Pending<I>,
    ) where
        I: Copy + Ord + Hash,
        E: Equation<U, I>,
    {
        if let Some((a, b)) = pending.pop() {
            self.merge(a, b, union_find, use_set, pending)
        }
    }

    /// Check this data structure's invariants w.r.t a union-find ADT
    pub fn check_invariants<U, I>(&self, union_find: &U, use_set: &UseSet<I>) -> bool
    where
        I: Copy + Hash + Ord,
        E: Equation<U, I>,
        U: UnionFind<I>,
    {
        let mut seen = HashSet::with_capacity(self.lookup.len());
        for &(ix, user_hash) in use_set.0.iter() {
            if let Some(eqn) = self.lookup.get(user_hash, |eqn| {
                eqn.visit_deps(
                    &mut |dsf, i| {
                        if dsf.find(i) == ix {
                            Err(())
                        } else {
                            Ok(())
                        }
                    },
                    union_find,
                )
                .is_err()
            }) {
                seen.insert(eqn as *const _);
            } else {
                return false;
            }
        }
        //SAFETY: lookup is not dropped or modified for the duration of this block
        unsafe {
            for lookup in self.lookup.iter() {
                if !seen.contains(&(lookup.as_ptr() as *const _)) {
                    return false;
                }
            }
        }
        true
    }
}

/// A set of `(used, user)` pairs, where `used` is an index in a context and `user` is an equation hash modulo that context
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct UseSet<I = usize>(BTreeSet<(I, u64)>);

impl<I> UseSet<I>
where
    I: Copy + Ord,
{
    /// Create a new, empty `UseSet` instance
    ///
    /// Does not allocate
    #[inline(always)]
    pub fn new() -> UseSet<I> {
        UseSet(BTreeSet::new())
    }

    /// Get the length of this use set
    #[inline(always)]
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Whether this use set is empty
    #[inline(always)]
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Clear this use set
    #[inline(always)]
    pub fn clear(&mut self) {
        self.0.clear()
    }

    /// Given an index `used: I`, remove the pair `(used, user)` with the smallest possible value of `used` greater than `min` if such a pair exists,
    /// and return the value `used`. If no such pair exists, return `None`.
    #[inline]
    fn pop(&mut self, used: I, min: u64) -> Option<u64> {
        let to_remove = *self.0.range((used, min)..=(used, u64::MAX)).next()?;
        let removed = self.0.remove(&to_remove);
        debug_assert!(removed);
        debug_assert!(to_remove.0 == used);
        Some(to_remove.1)
    }

    /// Insert a new `(used, user)` pair into this set
    #[inline(always)]
    fn insert(&mut self, used: I, user_hash: u64) {
        self.0.insert((used, user_hash));
    }

    /// Insert a remove the pair `(used, user)` from this set
    #[inline(always)]
    fn remove(&mut self, used: I, user_hash: u64) {
        self.0.remove(&(used, user_hash));
    }
}

impl<I> Default for UseSet<I> {
    #[inline(always)]
    fn default() -> Self {
        UseSet(BTreeSet::default())
    }
}

/// A list of pending equalities for congruence closure
#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct Pending<I = usize>(Vec<(I, I)>);

impl<I> Pending<I> {
    /// Creates a new, empty pending list.
    ///
    /// Does not allocate.
    #[inline(always)]
    pub const fn new() -> Pending<I> {
        Pending(Vec::new())
    }

    /// Creates a new pending list with the given capacity.
    #[inline(always)]
    pub fn with_capacity(cap: usize) -> Pending<I> {
        Pending(Vec::with_capacity(cap))
    }

    /// Get the length of this pending list
    #[inline(always)]
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Get the capacity of this pending list
    #[inline(always)]
    pub fn capacity(&self) -> usize {
        self.0.capacity()
    }

    /// Whether this pending list is empty
    #[inline(always)]
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Get the next equality in this pending list, if any
    #[inline(always)]
    pub fn pop(&mut self) -> Option<(I, I)> {
        self.0.pop()
    }

    /// Add a pending equality to the list
    #[inline(always)]
    pub fn push(&mut self, left: I, right: I) {
        self.0.push((left, right))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use rand::prelude::SliceRandom;
    use rand::Rng;

    #[test]
    fn congruence_chain() {
        let mut dsf = DisjointSetForest::with_capacity(10);
        let mut cc = CongruenceClosure::new();
        let mut us = UseSet::new();
        let mut cs = Pending::new();

        let (a, b, c, d, e, f, g, h) = (0, 1, 2, 3, 4, 5, 6, 7);

        // Set a * b ~ c
        cc.equation(Equate((a, b), c), &mut dsf, &mut us, &mut cs);
        // Set a * d ~ e
        cc.equation(Equate((a, d), e), &mut dsf, &mut us, &mut cs);
        // Set f * c ~ g
        cc.equation(Equate((f, c), g), &mut dsf, &mut us, &mut cs);
        // Set f * e ~ h
        cc.equation(Equate((f, e), h), &mut dsf, &mut us, &mut cs);

        assert!(dsf.is_empty());

        // If b ~ d, then a * b ~ c ~ a * d ~ e, and therefore f * c ~ g ~ f * e ~ h
        cc.merge(b, d, &mut dsf, &mut us, &mut cs);
        assert!(!dsf.is_empty());
        assert!(dsf.node_eq_mut(b, d));
        assert!(dsf.node_eq_mut(c, e));
        assert!(dsf.node_eq_mut(g, h));
        assert!(!dsf.node_eq_mut(a, b));
        assert!(!dsf.node_eq_mut(a, c));
        assert!(!dsf.node_eq_mut(a, g));
        assert!(!dsf.node_eq_mut(b, c));
        assert!(!dsf.node_eq_mut(b, g));
        assert!(!dsf.node_eq_mut(c, g));
    }

    #[test]
    fn invariants() {
        let mut dsf = DisjointSetForest::with_capacity(3);
        let mut cc = CongruenceClosure::new();
        let mut us = UseSet::new();
        let mut st = Pending::new();

        cc.merge(0, 1, &mut dsf, &mut us, &mut st);
        debug_assert!(cc.check_invariants(&mut dsf, &mut us));
        cc.merge(0, 2, &mut dsf, &mut us, &mut st);
        debug_assert!(cc.check_invariants(&mut dsf, &mut us));
        cc.equation(Equate((0, 1), 0), &mut dsf, &mut us, &mut st);
        debug_assert!(cc.check_invariants(&mut dsf, &mut us));
        cc.equation(Equate((2, 3), 4), &mut dsf, &mut us, &mut st);
        debug_assert!(cc.check_invariants(&mut dsf, &mut us));
        cc.equation(Equate((0, 1), 0), &mut dsf, &mut us, &mut st);
        debug_assert!(cc.check_invariants(&mut dsf, &mut us));
        cc.merge(3, 5, &mut dsf, &mut us, &mut st);
        debug_assert!(cc.check_invariants(&mut dsf, &mut us));
        cc.merge(5, 0, &mut dsf, &mut us, &mut st);
        debug_assert!(cc.check_invariants(&mut dsf, &mut us));
        cc.equation(Equate((3, 5), 5), &mut dsf, &mut us, &mut st);
        debug_assert!(cc.check_invariants(&mut dsf, &mut us));
    }

    // Ignore in MIRI because too slow
    #[test]
    #[cfg_attr(miri, ignore)]
    fn congruence_closure_order_and_join() {
        let mut rng = rand::thread_rng();
        let mut dsf = DisjointSetForest::with_capacity(3);
        let mut cc_dsf = DisjointSetForest::with_capacity(30);
        let mut cc_end_dsf = DisjointSetForest::with_capacity(64);
        let mut cc = CongruenceClosure::new();
        let mut cc_end = CongruenceClosure::new();
        let mut us = UseSet::new();
        let mut us_end = UseSet::new();
        let mut st = Pending::default();
        let mut equations = Vec::with_capacity(4096);

        for _ in 0..2048 {
            let a = rng.gen::<usize>() % 256;
            let b = rng.gen::<usize>() % 256;
            dsf.union(a, b);
            cc_end.merge(a, b, &mut cc_end_dsf, &mut us_end, &mut st);
            cc.merge(a, b, &mut cc_dsf, &mut us, &mut st);
            let c = rng.gen::<usize>() % 256;
            let d = rng.gen::<usize>() % 256;
            dsf.union(c, d);
            cc_end.merge(c, d, &mut cc_end_dsf, &mut us_end, &mut st);
            cc.merge(c, d, &mut cc_dsf, &mut us, &mut st);
            let e = rng.gen::<usize>() % 256;
            let f = rng.gen::<usize>() % 256;
            cc.equation(Equate((a, b), c), &mut cc_dsf, &mut us, &mut st);
            cc.equation(Equate((d, e), f), &mut cc_dsf, &mut us, &mut st);
            equations.push((a, b, c, d, e, f));
            let e = e + 256;
            let f = f + 256;
            cc.equation(Equate((a, b), c), &mut cc_dsf, &mut us, &mut st);
            cc.equation(Equate((d, e), f), &mut cc_dsf, &mut us, &mut st);
            equations.push((a, b, c, d, e, f));
        }
        assert!(dsf.refines_mut(&mut cc_dsf));
        assert!(!cc_dsf.refines_mut(&mut dsf));
        assert_eq!(dsf, cc_end_dsf);

        equations.shuffle(&mut rng);
        for (a, b, c, d, e, f) in equations {
            cc_end.equation(Equate((a, b), c), &mut cc_end_dsf, &mut us_end, &mut st);
            cc_end.equation(Equate((d, e), f), &mut cc_end_dsf, &mut us_end, &mut st);
        }
        assert_ne!(dsf, cc_end_dsf);
        assert_eq!(cc_dsf, cc_end_dsf);
        assert_ne!(dsf, cc_end_dsf);
    }
}
