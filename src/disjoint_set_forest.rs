/*!
A simple disjoint-set forest implementation implementing the `UnionFind` trait.
*/
use super::*;

/// A minimal [disjoint set forest](https://en.wikipedia.org/wiki/Disjoint-set_data_structure) with dense `usize` nodes, implementing the [`UnionFind`] trait.
///
/// Has support for up to `2**48` nodes, which should be enough to exhaust the entire address space of most modern CPUs.
///
/// # Examples
/// ```rust
/// # use congruence::{DisjointSetForest, UnionFind};
/// let mut dsf = DisjointSetForest::with_capacity(4);
/// for i in 0..3 {
///     for j in 0..3 {
///         assert_eq!(dsf.node_eq(i, j), i == j)
///     }
/// }
/// dsf.union(0, 1);
/// for i in 0..3 {
///     for j in 0..3 {
///         assert_eq!(
///             dsf.node_eq(i, j),
///             i == j || (i == 1 && j == 0) || (i == 0 && j == 1)
///         )
///     }
/// }
/// dsf.union(2, 3);
/// for i in 0..3 {
///     for j in 0..3 {
///         assert_eq!(
///             dsf.node_eq(i, j),
///             i == j
///                 || (i == 1 && j == 0)
///                 || (i == 0 && j == 1)
///                 || (i == 2 && j == 3)
///                 || (i == 3 && j == 2)
///         )
///     }
/// }
/// dsf.union(1, 2);
/// for i in 0..3 {
///     for j in 0..3 {
///         assert!(dsf.node_eq(i, j),)
///     }
/// }
/// ```
#[derive(Debug, Default, Clone)]
pub struct DisjointSetForest {
    nodes: Vec<Node>,
}

impl DisjointSetForest {
    /// Create a new, empty disjoint set forest. Guaranteed not to allocate.
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::DisjointSetForest;
    /// let dsf = DisjointSetForest::new();
    /// assert!(dsf.is_empty());
    /// assert_eq!(dsf.len(), 0);
    /// assert_eq!(dsf.capacity(), 0);
    /// ```
    #[inline(always)]
    pub const fn new() -> DisjointSetForest {
        DisjointSetForest { nodes: Vec::new() }
    }

    /// Create a new disjoint set forest with `n` nodes
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::DisjointSetForest;
    /// let dsf = DisjointSetForest::with_capacity(5);
    /// assert!(dsf.is_empty());
    /// assert_eq!(dsf.len(), 0);
    /// assert!(dsf.capacity() >= 5);
    /// ```
    #[inline]
    pub fn with_capacity(n: usize) -> DisjointSetForest {
        DisjointSetForest {
            nodes: Vec::with_capacity(n),
        }
    }

    /// Reserve capacity for `n` additional nodes
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::DisjointSetForest;
    /// let mut dsf = DisjointSetForest::new();
    /// assert!(dsf.is_empty());
    /// assert_eq!(dsf.capacity(), 0);
    /// assert_eq!(dsf.len(), 0);
    /// dsf.reserve(5);
    /// assert!(dsf.is_empty());
    /// assert!(dsf.capacity() >= 5);
    /// assert_eq!(dsf.len(), 0);
    /// ```
    #[inline(always)]
    pub fn reserve(&mut self, n: usize) {
        self.nodes.reserve(n);
    }

    /// Get whether this forest is empty, i.e. contains no *relations* between elements
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::DisjointSetForest;
    /// let mut dsf = DisjointSetForest::new();
    /// assert!(dsf.is_empty());
    /// ```
    #[inline(always)]
    pub fn is_empty(&self) -> bool {
        for (ix, node) in self.nodes.iter().enumerate() {
            if node.parent() != ix {
                return false;
            }
        }
        true
    }

    /// Get the current length of this forest, defined as 1 + the largest index either unioned with another index or with a nonzero flag set
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::new();
    /// assert_eq!(dsf.len(), 0);
    /// dsf.union(2, 3);
    /// assert_eq!(dsf.len(), 4);
    /// dsf.set_flags(5, 0b0);
    /// assert_eq!(dsf.len(), 4);
    /// dsf.set_flags(5, 0b1);
    /// assert_eq!(dsf.len(), 6);
    /// ```
    #[inline(always)]
    pub fn len(&self) -> usize {
        self.nodes.len()
    }

    /// Get the current capacity of this forest
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::DisjointSetForest;
    /// let mut dsf = DisjointSetForest::new();
    /// assert_eq!(dsf.capacity(), 0);
    /// dsf.reserve(1);
    /// assert!(dsf.capacity() >= 1);
    /// dsf.reserve(5);
    /// assert!(dsf.capacity() >= 6);
    /// ```
    #[inline(always)]
    pub fn capacity(&self) -> usize {
        self.nodes.capacity()
    }

    /// Clear this forest, removing all nodes but preserving it's capacity
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::new();
    /// assert!(dsf.is_empty());
    /// assert!(!dsf.node_eq(5, 3));
    /// assert_eq!(dsf.capacity(), 0);
    /// dsf.union(5, 3);
    /// assert!(!dsf.is_empty());
    /// assert!(dsf.node_eq(5, 3));
    /// let capacity = dsf.capacity();
    /// assert!(capacity >= 5);
    /// dsf.clear();
    /// assert!(dsf.is_empty());
    /// assert!(!dsf.node_eq(5, 3));
    /// assert_eq!(dsf.capacity(), capacity);
    /// ```
    #[inline(always)]
    pub fn clear(&mut self) {
        self.nodes.clear()
    }

    /// Get the flags for a node in this forest
    ///
    /// For usage examples, see [`Self::set_flags`].
    #[inline]
    pub fn flags(&self, node: usize) -> u8 {
        self.nodes
            .get(self.find(node))
            .unwrap_or(&Node::new(0))
            .flags()
    }

    /// Get the flags for a node in this forest
    ///
    /// Always returns the same result as [`Self::flags`], but performs path compression, leading to better amortized performance.
    /// For usage examples, see [`Self::set_flags`].
    #[inline]
    pub fn flags_mut(&mut self, node: usize) -> u8 {
        let ix = self.find_mut(node);
        self.nodes.get(ix).unwrap_or(&Node::new(0)).flags()
    }

    /// Set the flags for a node in this forest, returning the old value
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf = DisjointSetForest::new();
    ///
    /// assert_eq!(dsf.flags(2), 0b0);
    /// assert_eq!(dsf.flags_mut(2), 0b0);
    /// // Calling flags_mut on an out-of-bounds index, by definition, does not affect `len`
    /// assert_eq!(dsf.len(), 0);
    ///
    /// assert_eq!(dsf.set_flags(2, 0b1), 0b0);
    /// assert_eq!(dsf.len(), 3);
    /// assert_eq!(dsf.flags(2), 0b1);
    /// assert_eq!(dsf.set_flags(2, 0b10), 0b1);
    /// assert_eq!(dsf.len(), 3);
    /// // Calling `set_flags` sets the result to the bitwise OR of the old and new values
    /// assert_eq!(dsf.flags(2), 0b11);
    ///
    /// assert_eq!(dsf.set_flags(3, 0b100), 0b0);
    /// assert_eq!(dsf.len(), 4);
    /// assert_eq!(dsf.flags(2), 0b011);
    /// assert_eq!(dsf.flags(3), 0b100);
    ///
    /// // Likewise, the flag of the union of two nodes is the bitwise OR of the pre-union flags of those nodes
    /// dsf.union(2, 3);
    /// assert_eq!(dsf.len(), 4);
    /// assert_eq!(dsf.flags(2), 0b111);
    /// assert_eq!(dsf.flags(3), 0b111);
    /// ```
    #[inline]
    pub fn set_flags(&mut self, node: usize, flags: u8) -> u8 {
        if let Some(node) = self.nodes.get_mut(node) {
            let old_flags = node.flags();
            node.set_flags(flags);
            old_flags
        } else {
            if flags != 0 {
                self.nodes.reserve(node - self.nodes.len());
                while self.nodes.len() <= node {
                    self.insert()
                }
                self.nodes[node].set_flags(flags);
            }
            return 0b0;
        }
    }

    /// Insert a new node
    fn insert(&mut self) {
        self.nodes.push(Node::new(self.nodes.len()))
    }

    /// Get whether this disjoint set forest refines another, i.e., if `a ~ b` in `self`, then `a ~ b` in `other`
    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf_0 = DisjointSetForest::with_capacity(5);
    /// let mut dsf_1 = DisjointSetForest::with_capacity(5);
    /// assert!(dsf_0.refines_mut(&mut dsf_1));
    /// assert!(dsf_0.refines_mut(&mut dsf_1));
    /// dsf_0.union(3, 4);
    /// dsf_0.union(3, 2);
    /// assert!(!dsf_0.refines_mut(&mut dsf_1));
    /// assert!(dsf_1.refines_mut(&mut dsf_0));
    /// dsf_1.union(3, 4);
    /// assert!(!dsf_0.refines_mut(&mut dsf_1));
    /// assert!(dsf_1.refines_mut(&mut dsf_0));
    /// dsf_1.union(3, 5);
    /// assert!(!dsf_0.refines_mut(&mut dsf_1));
    /// assert!(!dsf_1.refines_mut(&mut dsf_0));
    /// dsf_1.union(3, 2);
    /// assert!(dsf_0.refines_mut(&mut dsf_1));
    /// assert!(!dsf_1.refines_mut(&mut dsf_0));
    /// dsf_0.union(3, 5);
    /// assert!(dsf_0.refines_mut(&mut dsf_1));
    /// assert!(dsf_1.refines_mut(&mut dsf_0));
    /// ```
    pub fn refines_mut(&mut self, other: &mut impl UnionFind) -> bool {
        for ix in 0..self.nodes.len() {
            if !other.node_eq_mut(ix, self.find_mut(ix)) {
                return false;
            }
        }
        true
    }

    /// Get whether this disjoint set forest refines another, i.e., if `a ~ b` in `self`, then `a ~ b` in `other`, without performing path compression
    ///
    /// Always returns the same result as [Self::`refines`], but does not optimize the data structure, leading to worse amortized performance.    ///
    /// # Examples
    /// ```rust
    /// # use congruence::{DisjointSetForest, UnionFind};
    /// let mut dsf_0 = DisjointSetForest::with_capacity(5);
    /// let mut dsf_1 = DisjointSetForest::with_capacity(5);
    /// assert!(dsf_0.refines(&mut dsf_1));
    /// assert!(dsf_0.refines(&mut dsf_1));
    /// dsf_0.union(3, 4);
    /// dsf_0.union(3, 2);
    /// assert!(!dsf_0.refines(&mut dsf_1));
    /// assert!(dsf_1.refines(&mut dsf_0));
    /// dsf_1.union(3, 4);
    /// assert!(!dsf_0.refines(&mut dsf_1));
    /// assert!(dsf_1.refines(&mut dsf_0));
    /// dsf_1.union(3, 5);
    /// assert!(!dsf_0.refines(&mut dsf_1));
    /// assert!(!dsf_1.refines(&mut dsf_0));
    /// dsf_1.union(3, 2);
    /// assert!(dsf_0.refines(&mut dsf_1));
    /// assert!(!dsf_1.refines(&mut dsf_0));
    /// dsf_0.union(3, 5);
    /// assert!(dsf_0.refines(&mut dsf_1));
    /// assert!(dsf_1.refines(&mut dsf_0));
    /// ```
    pub fn refines(&self, other: &DisjointSetForest) -> bool {
        for ix in 0..self.nodes.len() {
            if !other.node_eq(ix, self.find(ix)) {
                return false;
            }
        }
        true
    }

    /// Take the union of two representative nodes, returning the new root
    fn union_repr(&mut self, left: usize, right: usize) -> usize {
        let (parent, child, bump) = match self.nodes[left].rank().cmp(&self.nodes[right].rank()) {
            Ordering::Greater => (left, right, 0),
            Ordering::Equal => (left, right, 1),
            Ordering::Less => (right, left, 0),
        };
        self.nodes[child].set_parent(parent);
        self.nodes[parent].bump_rank(bump);
        let child_flags = self.nodes[child].flags();
        self.nodes[parent].set_flags(child_flags);
        parent
    }
}

impl PartialEq for DisjointSetForest {
    fn eq(&self, other: &Self) -> bool {
        self.refines(other) && other.refines(self)
    }
}

impl Eq for DisjointSetForest {}

impl UnionFind for DisjointSetForest {
    #[inline]
    fn union_find(&mut self, left: usize, right: usize) -> usize {
        if left >= self.nodes.len() || right >= self.nodes.len() {
            let max = left.max(right);
            self.nodes.reserve(1 + max - self.nodes.len());
            while max >= self.nodes.len() {
                self.insert()
            }
        }
        let left = self.find_mut(left);
        let right = self.find_mut(right);
        self.union_repr(left, right)
    }

    #[inline]
    fn pre_union_find(&mut self, left: usize, right: usize) -> usize {
        let left_repr = self.find_mut(left);
        if left == right {
            return left_repr;
        }
        let right_repr = self.find_mut(right);
        if self.nodes.get(left_repr).unwrap_or(&Node::new(0)).rank()
            >= self.nodes.get(right_repr).unwrap_or(&Node::new(0)).rank()
        {
            left_repr
        } else {
            right_repr
        }
    }

    #[inline]
    fn union(&mut self, left: usize, right: usize) {
        self.union_find(left, right);
    }

    #[inline]
    fn find(&self, mut ix: usize) -> usize {
        if ix >= self.nodes.len() {
            return ix;
        }
        while self.nodes[ix].parent() != ix {
            ix = self.nodes[ix].parent()
        }
        ix
    }

    #[inline]
    fn node_eq(&self, i: usize, j: usize) -> bool {
        if i == j {
            true
        } else {
            self.find(i) == self.find(j)
        }
    }

    #[inline]
    fn find_mut(&mut self, mut node: usize) -> usize {
        if node >= self.nodes.len() {
            return node;
        }
        loop {
            let parent = self.nodes[node].parent();
            if parent == node {
                return node;
            }
            let grandparent = self.nodes[parent].parent();
            self.nodes[node].set_parent(grandparent);
            node = parent;
        }
    }

    #[inline]
    fn node_eq_mut(&mut self, left: usize, right: usize) -> bool {
        if left == right {
            true
        } else {
            self.find_mut(left) == self.find_mut(right)
        }
    }
}

/// A node in a disjoint set forest. The first 8 bits are the node's rank, and the remaining bits are the node's parent
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
struct Node(u64);

impl Debug for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_tuple("Node")
            .field(&self.parent())
            .field(&self.rank())
            .field(&self.flags()) //TODO: debug as binary?
            .finish()
    }
}

impl Node {
    const RANK_SHIFT: u32 = 48;
    const FLAG_SHIFT: u32 = 56;
    const IX_MASK: u64 = (1 << Self::RANK_SHIFT) - 1;
    const RANK_MASK: u64 = ((1 << Self::FLAG_SHIFT) - 1) & !Self::IX_MASK;
    const FLAG_MASK: u64 = !(Self::RANK_MASK | Self::IX_MASK);

    #[inline(always)]
    fn new(parent: usize) -> Node {
        assert_eq!(
            parent as u64 & !Self::IX_MASK,
            0,
            "Overflowed maximum node capacity!"
        );
        Node(parent as u64)
    }

    #[inline(always)]
    fn set_parent(&mut self, ix: usize) {
        debug_assert_eq!(ix as u64 & !Self::IX_MASK, 0, "Set invalid parent {}", ix);
        self.0 = (self.0 & !Self::IX_MASK) | (ix as u64)
    }

    #[inline(always)]
    fn parent(&self) -> usize {
        (self.0 & Self::IX_MASK) as usize
    }

    #[inline(always)]
    fn rank(&self) -> u8 {
        ((self.0 & Self::RANK_MASK) >> Self::RANK_SHIFT) as u8
    }

    #[inline(always)]
    fn flags(&self) -> u8 {
        ((self.0 & Self::FLAG_MASK) >> Self::FLAG_SHIFT) as u8
    }

    #[inline(always)]
    fn bump_rank(&mut self, bump: u8) {
        self.0 = (self.0 & !Self::RANK_MASK)
            | (self.rank().saturating_add(bump) as u64) << Self::RANK_SHIFT;
    }

    #[inline(always)]
    fn set_flags(&mut self, flags: u8) {
        self.0 |= (flags as u64) << Self::FLAG_SHIFT
    }
}
