/*!
A trait for objects which may be used as a language for congruence closure
*/
use crate::{Pending, UnionFind};
use core::hash::{BuildHasher, Hash, Hasher};
use std::{borrow::Borrow, rc::Rc, sync::Arc};

/// An object which can be used as an equation between a term `L` and an index `x` of the form `L = x`
pub trait Equation<C, I>: Language<C, I> {
    /// Find a representative for the given index
    fn find(ix: I, ctx: &C) -> I;

    /// Find a representative for the given index
    fn find_mut(ix: I, ctx: &mut C) -> I;

    /// Check whether two indices are in the same equivalence class
    fn node_eq(left: I, right: I, ctx: &C) -> bool;

    /// Check whether two indices are in the same equivalence class
    fn node_eq_mut(left: I, right: I, ctx: &mut C) -> bool;

    /// Get the index on the right-hand side of this equation
    fn ix(&self, ctx: &C) -> Option<I>;

    /// Mutably get the index on the right-hand side of this equation
    fn ix_mut(&self, ctx: &mut C) -> Option<I>;

    /// Merge two indices in this system, returning the merged index and whether the new index requires an update.
    ///
    /// Any additional new merges required are pushed to the pending queue.
    fn merge_ix(left: I, right: I, ctx: &mut C, pending: &mut Pending<I>) -> (I, bool);

    /// Merge this equation with `other`, returning two indices to merge, if any.
    ///
    /// Any additional merges required (e.g., due to injectivity/irrelevance) are pushed to the pending queue.
    fn merge_pending(
        &mut self,
        other: Self,
        ctx: &mut C,
        pending: &mut Pending<I>,
    ) -> Option<(I, I)>;
}

/// A simple equation of the form `term = ix`
///
/// Note that, while this automatically implements [`Language`] by delegating to `L`, it is up to the user to implement [`Equation`] for this type depending
/// on the logic of `L`'s [`Language`] implementation.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Equate<L, I>(pub L, pub I);

/// An object which can be used as a language for congruence closure with indices of type `I` given a context of type `C`.
///
/// We assume that this language is compatible with some relation `⪯` on `C`, with each context `c: C` equipped with
/// a relation `~(c)` on `I` such that
/// ```text
/// ∀ c, c' ∈ C, c ⪯ c' ⟹ [∀ i, j ∈ I, i ~(c) j ⟹ i ~(c') j]
/// ```
pub trait Language<C, I>: Sized + EqMod<C, I> + HashMod<C, I> {
    /// Visit the dependencies of this term
    ///
    /// We define `Visited(x, c)` to be the set of indices `i` for which `visitor(c, i)` is called when we call `x.visit_deps(visitor, c)`
    ///
    /// In a valid implementation, this method must satisfy the following properties
    /// - `Visited(x, c)` is a function, i.e., if `x == x'` and `c == c'` then `Visited(x', c') == Visited(x, c)`
    ///   (as a set, i.e., modulo `Hash`, `Eq`, ordering, and multiplicity)
    /// - If `c ⪯ c'` and this implementation is compatible with `⪯`, then
    ///   ```text
    ///     ∀ i ∈ Visited(x, c'), ∃ j ∈ Visited(x, c), i ~(c') j
    ///   ```
    ///   i.e. using a *coarser* context never causes `visitor` to be called with *more* distinct elements.
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&C, I) -> Result<(), E>,
        ctx: &C,
    ) -> Result<(), E>;

    /// Visit the dependencies of this term, potentially optimizing the context during the access operation
    ///
    /// Must have the same behaviour, modulo mutability, as [`Self::visit_deps`]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut C, I) -> Result<(), E>,
        ctx: &mut C,
    ) -> Result<(), E>;
}

/// An type within which, when taken as a language, all objects have constant class
///
/// We say an object `x: L` has *constant class*, where `L: Language<C, I>`, if for all `c, c': C` such that `c ⪯ c'`,
/// - `Visited(x, c) == Visited(x, c')`, modulo `~(c)`
/// - If `x, y` are compatible, then `Compared(x, y, c) == Compared(x, y, c')` modulo `~(c)`
/// where `Visited` and `Compared` are defined as in [`Language::visit_deps`] and [`Language::eq_mod`] respectively.
pub trait ConstLanguage<C, I>: Language<C, I> {}

/// A type which can be compared to another modulo some equivalence context over a type `I`
///
/// We assume that this comparison is compatible with some relation `⪯` on `C`, with each context `c: C` equipped with
/// a relation `~(c)` on `I` such that
/// ```text
/// ∀ c, c' ∈ C, c ⪯ c' ⟹ [∀ i, j ∈ I, i ~(c) j ⟹ i ~(c') j]
/// ```
pub trait EqMod<C, I, O: ?Sized = Self> {
    /// Whether this object is equal to another modulo the given context
    ///
    /// We say `x` is *compatible* with `y` in the context `c` if `x.eq_mod(&y, |_, _, _| true, &c)`; otherwise, we say `x` is *incompatible* with `y`.
    /// We define `Checked(x, y, c)` to be the ordered list of elements `(i, j): (I, I)` on which `eq_mod(c, i, ij)` is called when we call
    /// `x.eq_mod(y, eq_mod, c)`. `Visited` is defined as in [`Language::visit_deps`].
    ///
    /// In a valid implementation, this method must satisfy the following properties:
    /// - `x` is compatible with itself (reflexivity)
    /// - `x.eq_mod(y, eq_mod, c) ⟺ y.eq_mod(x, eq_mod, c)` (symmetry)
    /// - If `Self` implements [`Language`], as a set, `Checked(x, y, c)` is a subset of `Visited(x, c) × Visited(y, c)`
    /// - If `x` is incompatible with `y`, then this method always returns `false`
    /// - If `x` is compatible with `y` and `eq_mod(c, i, j)` is true for `(i, j) ∈ Checked(x, y, c)`, then `x.eq_mod(y, eq_mod, c) == true`
    /// - If `c ⪯ c'` and this implementation is compatible with `⪯`, then
    ///   ```text
    ///   x.eq_mod(y, eq_mod, c) == true ⟹ x.eq_mod(y, eq_mod, c') == true
    ///   ```
    fn eq_mod(&self, other: &O, eq_mod: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool;

    /// Whether this object is equal to another modulo the given context
    ///
    /// Must have the same behaviour, modulo mutability, as [`Self::eq_mod_mut`]
    fn eq_mod_mut(
        &self,
        other: &O,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool;
}

/// A type which can be hashed modulo some equivalence context over a type `I`
///
/// We assume that this comparison is compatible with some relation `⪯` on `C`, with each context `c: C` equipped with
/// a relation `~(c)` on `I` such that
/// ```text
/// ∀ c, c' ∈ C, c ⪯ c' ⟹ [∀ i, j ∈ I, i ~(c) j ⟹ i ~(c') j]
/// ```
pub trait HashMod<C, I> {
    /// Hash this object modulo the given context
    ///
    /// Calling `x.hash_mod(hasher, c, i)` calls `hash_mod(hasher, c, i)` for some arbitrary subset `i ∈ Visitor(x, c)` along with hashing some
    /// arbitrary data using `hasher`, where `Visited` is defined as in [`Self::visit_deps`].
    ///
    /// If `Self` also implements [`EqMod<C, I>`], this method must satisfy the following property:
    /// If `x` is compatible with `y` and, for all `(i, j) ∈ Checked(c, i, j)`, where `Checked` is defined as in [`Self::eq_mod`],
    /// `hash_mod(hasher, c, i)` has the same behaviour as `hash_mod(hasher, c, j)` when `eq_mod(c, i, j) == true`, then
    /// `x.hash_mod(hasher, hash_mod, c)` has the same behaviour as `y.hash_mod(hasher, hash_mod, c)`.
    fn hash_mod<H: Hasher>(
        &self,
        hasher: &mut H,
        hash_mod: &mut impl FnMut(&mut H, &C, I),
        ctx: &C,
    );

    /// Hash this object modulo the given context
    ///
    /// Must have the same behaviour, modulo mutability, as [`Self::hash_mod`]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        hash_mod: &mut impl FnMut(&mut H, &mut C, I),
        ctx: &mut C,
    );

    /// Hash this object modulo the given context
    ///
    /// A helper function which should always be defined with equivalent behaviour to
    /// ```ignore
    /// let mut hasher = hasher.build_hasher();
    /// self.hash_mod(&mut hasher, hash_mod, ctx);
    /// return hasher.finish()
    /// ```
    fn hash_one_mod<B: BuildHasher>(
        &self,
        hasher: &B,
        hash_mod: &mut impl FnMut(&mut B::Hasher, &C, I),
        ctx: &C,
    ) -> u64 {
        let mut hasher = hasher.build_hasher();
        self.hash_mod(&mut hasher, hash_mod, ctx);
        hasher.finish()
    }

    /// Hash this object modulo the given context
    ///
    /// A helper function which should always be defined with equivalent behaviour to
    /// ```ignore
    /// let mut hasher = hasher.build_hasher();
    /// self.hash_mod_mut(&mut hasher, hash_mod, ctx);
    /// return hasher.finish()
    /// ```
    fn hash_one_mod_mut<B: BuildHasher>(
        &self,
        hasher: &B,
        hash_mod: &mut impl FnMut(&mut B::Hasher, &mut C, I),
        ctx: &mut C,
    ) -> u64 {
        let mut hasher = hasher.build_hasher();
        self.hash_mod_mut(&mut hasher, hash_mod, ctx);
        hasher.finish()
    }
}

impl<C, L, I> Language<C, I> for Equate<L, I>
where
    L: Language<C, I>,
{
    #[inline(always)]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&C, I) -> Result<(), E>,
        ctx: &C,
    ) -> Result<(), E> {
        self.0.visit_deps(visitor, ctx)
    }

    #[inline(always)]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut C, I) -> Result<(), E>,
        ctx: &mut C,
    ) -> Result<(), E> {
        self.0.visit_deps_mut(visitor, ctx)
    }
}

impl<L, I, C> ConstLanguage<C, I> for Equate<L, I> where L: ConstLanguage<C, I> {}

impl<C, L, I> EqMod<C, I> for Equate<L, I>
where
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &Self, eq_mod: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        self.0.eq_mod(&other.0, eq_mod, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &Self,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        self.0.eq_mod_mut(&other.0, map, ctx)
    }
}

impl<C, L, I> EqMod<C, I, L> for Equate<L, I>
where
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &L, eq_mod: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        self.0.eq_mod(other, eq_mod, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &L,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        self.0.eq_mod_mut(other, map, ctx)
    }
}

impl<C, L, I> EqMod<C, I, Arc<L>> for Equate<L, I>
where
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &Arc<L>, eq_mod: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        self.0.eq_mod(other, eq_mod, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &Arc<L>,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        self.0.eq_mod_mut(other, map, ctx)
    }
}

impl<C, L, I> EqMod<C, I, Rc<L>> for Equate<L, I>
where
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &Rc<L>, eq_mod: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        self.0.eq_mod(other, eq_mod, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &Rc<L>,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        self.0.eq_mod_mut(other, map, ctx)
    }
}

impl<C, L, I> EqMod<C, I, L> for Equate<Arc<L>, I>
where
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &L, eq_mod: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        self.0.eq_mod(other, eq_mod, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &L,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        self.0.eq_mod_mut(other, map, ctx)
    }
}

impl<C, L, I> EqMod<C, I, L> for Equate<Rc<L>, I>
where
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &L, eq_mod: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        self.0.eq_mod(other, eq_mod, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &L,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        self.0.eq_mod_mut(other, map, ctx)
    }
}

impl<C, L, I> HashMod<C, I> for Equate<L, I>
where
    L: Language<C, I>,
{
    #[inline(always)]
    fn hash_mod<H: Hasher>(
        &self,
        hasher: &mut H,
        hash_mod: &mut impl FnMut(&mut H, &C, I),
        ctx: &C,
    ) {
        self.0.hash_mod(hasher, hash_mod, ctx)
    }

    #[inline(always)]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        hash_mod: &mut impl FnMut(&mut H, &mut C, I),
        ctx: &mut C,
    ) {
        self.0.hash_mod_mut(hasher, hash_mod, ctx)
    }
}

impl<L, I, C> Language<C, I> for &'_ mut L
where
    L: Language<C, I>,
{
    #[inline(always)]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&C, I) -> Result<(), E>,
        ctx: &C,
    ) -> Result<(), E> {
        (**self).visit_deps(visitor, ctx)
    }

    #[inline(always)]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut C, I) -> Result<(), E>,
        ctx: &mut C,
    ) -> Result<(), E> {
        (**self).visit_deps_mut(visitor, ctx)
    }
}

impl<L, I, C> ConstLanguage<C, I> for &'_ mut L where L: ConstLanguage<C, I> {}

impl<L, I, C, B> EqMod<C, I, B> for &'_ mut L
where
    B: Borrow<L>,
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &B, map: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        (**self).eq_mod(other.borrow(), map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &B,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        (**self).eq_mod_mut(other.borrow(), map, ctx)
    }
}

impl<L, I, C> HashMod<C, I> for &'_ mut L
where
    L: HashMod<C, I>,
{
    #[inline(always)]
    fn hash_mod<H: Hasher>(&self, hasher: &mut H, map: &mut impl FnMut(&mut H, &C, I), ctx: &C) {
        (**self).hash_mod(hasher, map, ctx)
    }

    #[inline(always)]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &mut C, I),
        ctx: &mut C,
    ) {
        (**self).hash_mod_mut(hasher, map, ctx)
    }
}

impl<L, I, C> Language<C, I> for &'_ L
where
    L: Language<C, I>,
{
    #[inline(always)]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&C, I) -> Result<(), E>,
        ctx: &C,
    ) -> Result<(), E> {
        (**self).visit_deps(visitor, ctx)
    }

    #[inline(always)]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut C, I) -> Result<(), E>,
        ctx: &mut C,
    ) -> Result<(), E> {
        (**self).visit_deps_mut(visitor, ctx)
    }
}

impl<L, I, C> ConstLanguage<C, I> for &'_ L where L: ConstLanguage<C, I> {}

impl<L, I, C, B> EqMod<C, I, B> for &'_ L
where
    B: Borrow<L>,
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &B, map: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        (**self).eq_mod(other.borrow(), map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &B,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        (**self).eq_mod_mut(other.borrow(), map, ctx)
    }
}

impl<L, I, C> HashMod<C, I> for &'_ L
where
    L: HashMod<C, I>,
{
    #[inline(always)]
    fn hash_mod<H: Hasher>(&self, hasher: &mut H, map: &mut impl FnMut(&mut H, &C, I), ctx: &C) {
        (**self).hash_mod(hasher, map, ctx)
    }

    #[inline(always)]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &mut C, I),
        ctx: &mut C,
    ) {
        (**self).hash_mod_mut(hasher, map, ctx)
    }
}

impl<L, I, C> Language<C, I> for Box<L>
where
    L: Language<C, I>,
{
    #[inline(always)]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&C, I) -> Result<(), E>,
        ctx: &C,
    ) -> Result<(), E> {
        (**self).visit_deps(visitor, ctx)
    }

    #[inline(always)]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut C, I) -> Result<(), E>,
        ctx: &mut C,
    ) -> Result<(), E> {
        (**self).visit_deps_mut(visitor, ctx)
    }
}

impl<L, I, C, B> EqMod<C, I, B> for Box<L>
where
    B: Borrow<L>,
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &B, map: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        (**self).eq_mod(other.borrow(), map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &B,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        (**self).eq_mod_mut(other.borrow(), map, ctx)
    }
}

impl<L, I, C> ConstLanguage<C, I> for Box<L> where L: ConstLanguage<C, I> {}

impl<L, I, C> HashMod<C, I> for Box<L>
where
    L: HashMod<C, I>,
{
    #[inline(always)]
    fn hash_mod<H: Hasher>(&self, hasher: &mut H, map: &mut impl FnMut(&mut H, &C, I), ctx: &C) {
        (**self).hash_mod(hasher, map, ctx)
    }

    #[inline(always)]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &mut C, I),
        ctx: &mut C,
    ) {
        (**self).hash_mod_mut(hasher, map, ctx)
    }
}

impl<L, I, C> Language<C, I> for Arc<L>
where
    L: Language<C, I>,
{
    #[inline(always)]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&C, I) -> Result<(), E>,
        ctx: &C,
    ) -> Result<(), E> {
        (**self).visit_deps(visitor, ctx)
    }

    #[inline(always)]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut C, I) -> Result<(), E>,
        ctx: &mut C,
    ) -> Result<(), E> {
        (**self).visit_deps_mut(visitor, ctx)
    }
}

impl<L, I, C> ConstLanguage<C, I> for Arc<L> where L: ConstLanguage<C, I> {}

impl<L, I, C, B> EqMod<C, I, B> for Arc<L>
where
    B: Borrow<L>,
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &B, map: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        (**self).eq_mod(other.borrow(), map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &B,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        (**self).eq_mod_mut(other.borrow(), map, ctx)
    }
}

impl<L, I, C> HashMod<C, I> for Arc<L>
where
    L: HashMod<C, I>,
{
    #[inline(always)]
    fn hash_mod<H: Hasher>(&self, hasher: &mut H, map: &mut impl FnMut(&mut H, &C, I), ctx: &C) {
        (**self).hash_mod(hasher, map, ctx)
    }

    #[inline(always)]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &mut C, I),
        ctx: &mut C,
    ) {
        (**self).hash_mod_mut(hasher, map, ctx)
    }
}

impl<L, I, C> Language<C, I> for Rc<L>
where
    L: Language<C, I>,
{
    #[inline(always)]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&C, I) -> Result<(), E>,
        ctx: &C,
    ) -> Result<(), E> {
        (**self).visit_deps(visitor, ctx)
    }

    #[inline(always)]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut C, I) -> Result<(), E>,
        ctx: &mut C,
    ) -> Result<(), E> {
        (**self).visit_deps_mut(visitor, ctx)
    }
}

impl<L, I, C> ConstLanguage<C, I> for Rc<L> where L: ConstLanguage<C, I> {}

impl<L, I, C, B> EqMod<C, I, B> for Rc<L>
where
    B: Borrow<L>,
    L: EqMod<C, I>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &B, map: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        (**self).eq_mod(other.borrow(), map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &B,
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        (**self).eq_mod_mut(other.borrow(), map, ctx)
    }
}

impl<L, I, C> HashMod<C, I> for Rc<L>
where
    L: HashMod<C, I>,
{
    #[inline(always)]
    fn hash_mod<H: Hasher>(&self, hasher: &mut H, map: &mut impl FnMut(&mut H, &C, I), ctx: &C) {
        (**self).hash_mod(hasher, map, ctx)
    }

    #[inline(always)]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &mut C, I),
        ctx: &mut C,
    ) {
        (**self).hash_mod_mut(hasher, map, ctx)
    }
}

impl<A, B, C, I> Language<C, I> for (A, B)
where
    A: Language<C, I>,
    B: Language<C, I>,
{
    #[inline(always)]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&C, I) -> Result<(), E>,
        ctx: &C,
    ) -> Result<(), E> {
        self.0.visit_deps(visitor, ctx)?;
        self.1.visit_deps(visitor, ctx)
    }

    #[inline]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut C, I) -> Result<(), E>,
        ctx: &mut C,
    ) -> Result<(), E> {
        self.0.visit_deps_mut(visitor, ctx)?;
        self.1.visit_deps_mut(visitor, ctx)
    }
}

impl<A, B, C, I> ConstLanguage<C, I> for (A, B)
where
    A: ConstLanguage<C, I>,
    B: ConstLanguage<C, I>,
{
}

impl<A0, A1, B0, B1, C, I> EqMod<C, I, (A1, B1)> for (A0, B0)
where
    A0: EqMod<C, I, A1>,
    B0: EqMod<C, I, B1>,
{
    #[inline(always)]
    fn eq_mod(&self, other: &(A1, B1), map: &mut impl FnMut(&C, I, I) -> bool, ctx: &C) -> bool {
        self.0.eq_mod(&other.0, map, ctx) && self.1.eq_mod(&other.1, map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &(A1, B1),
        map: &mut impl FnMut(&mut C, I, I) -> bool,
        ctx: &mut C,
    ) -> bool {
        self.0.eq_mod_mut(&other.0, map, ctx) && self.1.eq_mod_mut(&other.1, map, ctx)
    }
}

impl<A, B, C, I> HashMod<C, I> for (A, B)
where
    A: HashMod<C, I>,
    B: HashMod<C, I>,
{
    #[inline(always)]
    fn hash_mod<H: Hasher>(&self, hasher: &mut H, map: &mut impl FnMut(&mut H, &C, I), ctx: &C) {
        self.0.hash_mod(hasher, map, ctx);
        self.1.hash_mod(hasher, map, ctx)
    }

    #[inline(always)]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &mut C, I),
        ctx: &mut C,
    ) {
        self.0.hash_mod_mut(hasher, map, ctx);
        self.1.hash_mod_mut(hasher, map, ctx)
    }
}

impl<U: UnionFind<usize>> Language<U, usize> for usize {
    #[inline]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut U, usize) -> Result<(), E>,
        ctx: &mut U,
    ) -> Result<(), E> {
        let ix = ctx.find_mut(*self);
        visitor(ctx, ix)
    }

    #[inline]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&U, usize) -> Result<(), E>,
        ctx: &U,
    ) -> Result<(), E> {
        let ix = ctx.find(*self);
        visitor(ctx, ix)
    }
}

impl<U: UnionFind<usize>> ConstLanguage<U, usize> for usize {}

impl<U: UnionFind<usize>> EqMod<U, usize, usize> for usize {
    #[inline]
    fn eq_mod(
        &self,
        other: &Self,
        eq_mod: &mut impl FnMut(&U, usize, usize) -> bool,
        ctx: &U,
    ) -> bool {
        eq_mod(ctx, *self, *other)
    }

    #[inline]
    fn eq_mod_mut(
        &self,
        other: &Self,
        eq_mod: &mut impl FnMut(&mut U, usize, usize) -> bool,
        ctx: &mut U,
    ) -> bool {
        eq_mod(ctx, *self, *other)
    }
}

impl<U: UnionFind<usize>> HashMod<U, usize> for usize {
    #[inline]
    fn hash_mod<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &U, usize),
        ctx: &U,
    ) {
        map(hasher, ctx, *self).hash(hasher)
    }

    #[inline]
    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &mut U, usize),
        ctx: &mut U,
    ) {
        map(hasher, ctx, *self)
    }
}

impl<U, I, L> Equation<U, I> for Equate<L, I>
where
    L: ConstLanguage<U, I>,
    U: UnionFind<I>,
    I: Copy,
{
    #[inline(always)]
    fn find(node: I, ctx: &U) -> I {
        ctx.find(node)
    }

    #[inline(always)]
    fn find_mut(node: I, ctx: &mut U) -> I {
        ctx.find_mut(node)
    }

    #[inline(always)]
    fn node_eq(left: I, right: I, ctx: &U) -> bool {
        ctx.node_eq(left, right)
    }

    #[inline(always)]
    fn node_eq_mut(left: I, right: I, ctx: &mut U) -> bool {
        ctx.node_eq_mut(left, right)
    }

    #[inline(always)]
    fn ix(&self, _ctx: &U) -> Option<I> {
        Some(self.1)
    }

    #[inline(always)]
    fn ix_mut(&self, _ctx: &mut U) -> Option<I> {
        Some(self.1)
    }

    #[inline(always)]
    fn merge_ix(left: I, right: I, ctx: &mut U, _pending: &mut Pending<I>) -> (I, bool) {
        let repr = ctx.union_find(left, right);
        (repr, false)
    }

    #[inline(always)]
    fn merge_pending(
        &mut self,
        other: Self,
        _ctx: &mut U,
        _pending: &mut Pending<I>,
    ) -> Option<(I, I)> {
        Some((self.1, other.1))
    }
}
