use std::{
    hash::{Hash, Hasher},
    rc::Rc,
};

use congruence::{
    CongruenceClosure, DisjointSetForest, EqMod, Equation, HashMod, Language, Pending, UnionFind,
    UseSet,
};
use indexmap::{map::Entry, IndexMap};

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum SymbolTree {
    App(Rc<SymbolTree>, Rc<SymbolTree>),
    Pair(Rc<SymbolTree>, Rc<SymbolTree>),
    Atom(String),
}

macro_rules! sym {
    (($l:tt, $r:tt)) => {
        Rc::new(SymbolTree::App(sym!($l), sym!($r)))
    };
    ([$l:tt, $r:tt]) => {
        Rc::new(SymbolTree::Pair(sym!($l), sym!($r)))
    };
    ($e:literal) => {
        Rc::new(SymbolTree::Atom(String::from($e)))
    };
    ($v:expr) => {
        ($v).clone()
    };
}

impl SymbolTree {
    pub fn visit_children(&self, visitor: &mut impl FnMut(Rc<SymbolTree>)) {
        match self {
            SymbolTree::App(left, right) | SymbolTree::Pair(left, right) => {
                visitor(left.clone());
                visitor(right.clone());
            }
            _ => {}
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum FunctionKind {
    /// A constant function: f x = f y for all x, y
    Constant = 0b1,
}

#[derive(Debug, Default)]
pub struct SymbolTreeCongruence {
    intern: SymbolTreeIntern,
    cc: CongruenceClosure<Rc<SymbolTree>>,
    us: UseSet,
    cs: Pending,
}

impl SymbolTreeCongruence {
    pub fn insert(&mut self, tree: Rc<SymbolTree>) -> (usize, bool) {
        let ix = match self.intern.table.entry(tree.clone()) {
            Entry::Occupied(o) => return (o.index(), false),
            Entry::Vacant(v) => {
                let ix = v.index();
                v.insert(());
                ix
            }
        };
        tree.visit_children(&mut |child| {
            self.insert(child);
        });
        self.cc
            .equation(tree, &mut self.intern, &mut self.us, &mut self.cs);
        (ix, true)
    }

    pub fn kind(&self, tree: &SymbolTree) -> Option<FunctionKind> {
        self.intern.kind(tree)
    }

    pub fn set_kind(&mut self, tree: Rc<SymbolTree>, kind: FunctionKind) -> Result<(), ()> {
        let (ix, _new) = self.insert(tree);
        let flags = self.intern.dsf.flags(ix);
        if flags & !(kind as u8) == 0 {
            let old = self.intern.dsf.set_flags(ix, kind as u8);
            if old != kind as u8 {
                self.cc
                    .update(ix, &mut self.intern, &mut self.us, &mut self.cs)
            }
            Ok(())
        } else {
            Err(())
        }
    }

    pub fn clear(&mut self) {
        self.intern.table.clear();
        self.intern.dsf.clear();
        self.cc.clear();
    }

    pub fn merge(&mut self, left: Rc<SymbolTree>, right: Rc<SymbolTree>) {
        let left_ix = self.insert(left).0;
        let right_ix = self.insert(right).0;
        self.cc.merge(
            left_ix,
            right_ix,
            &mut self.intern,
            &mut self.us,
            &mut self.cs,
        )
    }

    pub fn cong(&self, left: &SymbolTree, right: &SymbolTree) -> bool {
        (match (self.intern.ix(left), self.intern.ix(right)) {
            (Some(left), Some(right)) => return self.intern.dsf.node_eq(left, right),
            (None, Some(right)) => self.cc.expr_cong(left, right, &self.intern),
            (Some(left), None) => self.cc.expr_cong(right, left, &self.intern),
            (None, None) => false,
        }) || (match (left, right) {
            (SymbolTree::App(ll, lr), SymbolTree::App(rl, rr)) => {
                self.cong(ll, rl)
                    && (self.kind(ll) == Some(FunctionKind::Constant)
                        || self.kind(rl) == Some(FunctionKind::Constant)
                        || self.cong(lr, rr))
            }
            (SymbolTree::Pair(ll, lr), SymbolTree::Pair(rl, rr)) => {
                self.cong(ll, rl) && self.cong(lr, rr)
            }
            _ => left == right,
        })
    }

    pub fn cong_mut(&mut self, left: &SymbolTree, right: &SymbolTree) -> bool {
        (match (self.intern.ix(left), self.intern.ix(right)) {
            (Some(left), Some(right)) => return self.intern.dsf.node_eq_mut(left, right),
            (None, Some(right)) => self.cc.expr_cong_mut(left, right, &mut self.intern),
            (Some(left), None) => self.cc.expr_cong_mut(right, left, &mut self.intern),
            (None, None) => false,
        }) || (match (left, right) {
            (SymbolTree::App(ll, lr), SymbolTree::App(rl, rr)) => {
                self.cong_mut(ll, rl)
                    && (self.kind(ll) == Some(FunctionKind::Constant)
                        || self.kind(rl) == Some(FunctionKind::Constant)
                        || self.cong_mut(lr, rr))
            }
            (SymbolTree::Pair(ll, lr), SymbolTree::Pair(rl, rr)) => {
                self.cong_mut(ll, rl) && self.cong_mut(lr, rr)
            }
            _ => left == right,
        })
    }
}

#[derive(Debug, Default)]
pub struct SymbolTreeIntern {
    table: IndexMap<Rc<SymbolTree>, ()>,
    dsf: DisjointSetForest,
}

impl SymbolTreeIntern {
    pub fn ix(&self, tree: &SymbolTree) -> Option<usize> {
        self.table.get_index_of(tree)
    }

    pub fn kind(&self, tree: &SymbolTree) -> Option<FunctionKind> {
        match self.dsf.flags(self.ix(tree)?) {
            0b1 => Some(FunctionKind::Constant),
            0b0 => None,
            _ => unreachable!(),
        }
    }
}

impl Equation<SymbolTreeIntern, usize> for Rc<SymbolTree> {
    #[inline(always)]
    fn find(ix: usize, ctx: &SymbolTreeIntern) -> usize {
        ctx.find(ix)
    }

    #[inline(always)]
    fn find_mut(ix: usize, ctx: &mut SymbolTreeIntern) -> usize {
        ctx.find_mut(ix)
    }

    #[inline(always)]
    fn node_eq(left: usize, right: usize, ctx: &SymbolTreeIntern) -> bool {
        ctx.node_eq(left, right)
    }

    #[inline(always)]
    fn node_eq_mut(left: usize, right: usize, ctx: &mut SymbolTreeIntern) -> bool {
        ctx.node_eq_mut(left, right)
    }

    #[inline(always)]
    fn ix(&self, ctx: &SymbolTreeIntern) -> Option<usize> {
        ctx.table.get_index_of(self)
    }

    #[inline(always)]
    fn ix_mut(&self, ctx: &mut SymbolTreeIntern) -> Option<usize> {
        ctx.table.get_index_of(self)
    }

    #[inline(always)]
    fn merge_ix(
        left: usize,
        right: usize,
        ctx: &mut SymbolTreeIntern,
        _pending: &mut Pending<usize>,
    ) -> (usize, bool) {
        let left_repr = ctx.find_mut(left);
        let right_repr = ctx.find_mut(right);
        let left_flags = ctx.dsf.flags(left_repr);
        let right_flags = ctx.dsf.flags(right_repr);
        let repr = ctx.union_find(left_repr, right_repr);
        let flags = ctx.dsf.flags(repr);
        (
            repr,
            repr == left_repr && left_flags < flags || repr == right_repr && right_flags < flags,
        )
    }

    fn merge_pending(
        &mut self,
        other: Self,
        ctx: &mut SymbolTreeIntern,
        _pending: &mut Pending<usize>,
    ) -> Option<(usize, usize)> {
        Some((self.ix(ctx)?, other.ix(ctx)?))
    }
}

impl Language<SymbolTreeIntern, usize> for SymbolTree {
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&SymbolTreeIntern, usize) -> Result<(), E>,
        ctx: &SymbolTreeIntern,
    ) -> Result<(), E> {
        match self {
            SymbolTree::App(left, right) => {
                if let Some(left) = ctx.ix(left) {
                    visitor(ctx, left)?;
                }
                if let Some(right) = ctx.ix(right) {
                    visitor(ctx, right)?
                }
            }
            _ => {}
        }
        Ok(())
    }

    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut SymbolTreeIntern, usize) -> Result<(), E>,
        ctx: &mut SymbolTreeIntern,
    ) -> Result<(), E> {
        match self {
            SymbolTree::App(left, right) => {
                if let Some(left) = ctx.ix(left) {
                    visitor(ctx, left)?;
                }
                if let Some(right) = ctx.ix(right) {
                    visitor(ctx, right)?
                }
            }
            _ => {}
        }
        Ok(())
    }
}

impl EqMod<SymbolTreeIntern, usize, SymbolTree> for SymbolTree {
    fn eq_mod(
        &self,
        other: &Self,
        map: &mut impl FnMut(&SymbolTreeIntern, usize, usize) -> bool,
        ctx: &SymbolTreeIntern,
    ) -> bool {
        match (self, other) {
            (SymbolTree::App(this_l, this_r), SymbolTree::App(other_l, other_r)) => {
                (if let (Some(this_l), Some(other_l)) = (ctx.ix(this_l), ctx.ix(other_l)) {
                    map(ctx, this_l, other_l)
                } else {
                    this_l == other_l
                }) && (ctx.kind(this_l) == Some(FunctionKind::Constant)
                    || ctx.kind(other_l) == Some(FunctionKind::Constant)
                    || (if let (Some(this_r), Some(other_r)) = (ctx.ix(this_r), ctx.ix(other_r)) {
                        map(ctx, this_r, other_r)
                    } else {
                        this_r == other_r
                    }))
            }
            (SymbolTree::Pair(this_l, this_r), SymbolTree::Pair(other_l, other_r)) => {
                (if let (Some(this_l), Some(other_l)) = (ctx.ix(this_l), ctx.ix(other_l)) {
                    map(ctx, this_l, other_l)
                } else {
                    this_l == other_l
                }) && (if let (Some(this_r), Some(other_r)) = (ctx.ix(this_r), ctx.ix(other_r)) {
                    map(ctx, this_r, other_r)
                } else {
                    this_r == other_r
                })
            }
            (SymbolTree::Atom(this), SymbolTree::Atom(other)) => this == other,
            _ => false,
        }
    }

    fn eq_mod_mut(
        &self,
        other: &Self,
        map: &mut impl FnMut(&mut SymbolTreeIntern, usize, usize) -> bool,
        ctx: &mut SymbolTreeIntern,
    ) -> bool {
        match (self, other) {
            (SymbolTree::App(this_l, this_r), SymbolTree::App(other_l, other_r)) => {
                (if let (Some(this_l), Some(other_l)) = (ctx.ix(this_l), ctx.ix(other_l)) {
                    map(ctx, this_l, other_l)
                } else {
                    this_l == other_l
                }) && (ctx.kind(this_l) == Some(FunctionKind::Constant)
                    || ctx.kind(other_l) == Some(FunctionKind::Constant)
                    || (if let (Some(this_r), Some(other_r)) = (ctx.ix(this_r), ctx.ix(other_r)) {
                        map(ctx, this_r, other_r)
                    } else {
                        this_r == other_r
                    }))
            }
            (SymbolTree::Pair(this_l, this_r), SymbolTree::Pair(other_l, other_r)) => {
                (if let (Some(this_l), Some(other_l)) = (ctx.ix(this_l), ctx.ix(other_l)) {
                    map(ctx, this_l, other_l)
                } else {
                    this_l == other_l
                }) && (if let (Some(this_r), Some(other_r)) = (ctx.ix(this_r), ctx.ix(other_r)) {
                    map(ctx, this_r, other_r)
                } else {
                    this_r == other_r
                })
            }
            (SymbolTree::Atom(this), SymbolTree::Atom(other)) => this == other,
            _ => false,
        }
    }
}

impl HashMod<SymbolTreeIntern, usize> for SymbolTree {
    fn hash_mod<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &SymbolTreeIntern, usize),
        ctx: &SymbolTreeIntern,
    ) {
        std::mem::discriminant(self).hash(hasher);
        match self {
            SymbolTree::App(left, right) => {
                if let Some(left) = ctx.ix(left) {
                    map(hasher, ctx, left);
                } else {
                    left.hash(hasher);
                }
                if ctx.kind(left) != Some(FunctionKind::Constant) {
                    if let Some(right) = ctx.ix(right) {
                        map(hasher, ctx, right);
                    } else {
                        right.hash(hasher);
                    }
                }
            }
            SymbolTree::Pair(left, right) => {
                if let Some(left) = ctx.ix(left) {
                    map(hasher, ctx, left);
                } else {
                    left.hash(hasher);
                }
                if let Some(right) = ctx.ix(right) {
                    map(hasher, ctx, right);
                } else {
                    right.hash(hasher);
                }
            }
            SymbolTree::Atom(atom) => atom.hash(hasher),
        }
    }

    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        map: &mut impl FnMut(&mut H, &mut SymbolTreeIntern, usize),
        ctx: &mut SymbolTreeIntern,
    ) {
        std::mem::discriminant(self).hash(hasher);
        match self {
            SymbolTree::App(left, right) => {
                if let Some(left) = ctx.ix(left) {
                    map(hasher, ctx, left);
                } else {
                    left.hash(hasher);
                }
                if ctx.kind(left) != Some(FunctionKind::Constant) {
                    if let Some(right) = ctx.ix(right) {
                        map(hasher, ctx, right);
                    } else {
                        right.hash(hasher);
                    }
                }
            }
            SymbolTree::Pair(left, right) => {
                if let Some(left) = ctx.ix(left) {
                    map(hasher, ctx, left);
                } else {
                    left.hash(hasher);
                }
                if let Some(right) = ctx.ix(right) {
                    map(hasher, ctx, right);
                } else {
                    right.hash(hasher);
                }
            }
            SymbolTree::Atom(atom) => atom.hash(hasher),
        }
    }
}

impl UnionFind for SymbolTreeIntern {
    fn union_find(&mut self, left: usize, right: usize) -> usize {
        self.dsf.union_find(left, right)
    }

    fn pre_union_find(&mut self, left: usize, right: usize) -> usize {
        self.dsf.pre_union_find(left, right)
    }

    fn union(&mut self, left: usize, right: usize) {
        self.dsf.union(left, right)
    }

    fn find(&self, node: usize) -> usize {
        self.dsf.find(node)
    }

    fn node_eq(&self, left: usize, right: usize) -> bool {
        self.dsf.node_eq(left, right)
    }

    fn find_mut(&mut self, node: usize) -> usize {
        self.dsf.find_mut(node)
    }

    fn node_eq_mut(&mut self, left: usize, right: usize) -> bool {
        self.dsf.node_eq_mut(left, right)
    }
}

#[test]
fn basic_test() {
    let mut cc = SymbolTreeCongruence::default();
    let f = sym!("f");
    let g = sym!("g");
    let x = sym!("x");
    let y = sym!("y");
    let f_x = sym!((f, x));
    let g_y = sym!((g, y));
    let g_x = sym!((g, x));
    let f_x_y = sym!((f_x, y));
    let f_x_y_p = sym!([f_x, y]);
    assert!(!cc.cong_mut(&f_x, &g));
    assert!(!cc.cong_mut(&g_y, &y));
    assert!(!cc.cong_mut(&g_x, &x));
    assert!(!cc.cong_mut(&f_x_y, &y));
    cc.merge(f_x.clone(), g.clone());
    assert!(cc.cong_mut(&f_x, &g));
    assert!(!cc.cong_mut(&g_y, &y));
    cc.merge(g_y.clone(), y.clone());
    assert!(cc.cong_mut(&f_x, &g));
    assert!(cc.cong_mut(&g_y, &y));
    assert!(!cc.cong_mut(&g_x, &x));
    assert!(cc.cong_mut(&f_x_y, &y));
    assert!(!cc.cong_mut(&f_x_y_p, &y));
    cc.insert(f_x_y.clone());
    assert!(!cc.cong_mut(&g_x, &x));
    assert!(cc.cong_mut(&f_x_y, &y));
    assert!(!cc.cong_mut(&f_x_y_p, &y));
}

#[test]
fn g_x_tower() {
    let mut cc = SymbolTreeCongruence::default();
    let g = sym!("g");
    let x = sym!("x");
    let app_1 = sym!((g, x));
    let app_2 = sym!((g, (g, x)));
    let app_3 = sym!((g, (g, (g, x))));
    let app_5 = sym!((g, (g, (g, (g, (g, x))))));
    cc.merge(app_3.clone(), x.clone());
    assert!(!cc.cong_mut(&app_1, &x));
    assert!(cc.cong_mut(&app_3, &x));
    assert!(!cc.cong_mut(&app_5, &x));
    assert!(cc.cong_mut(&app_5, &app_2));
    cc.merge(app_5.clone(), x.clone());
    assert!(cc.cong_mut(&app_1, &x));
    assert!(cc.cong_mut(&app_2, &x));
    assert!(cc.cong_mut(&app_3, &x));
    assert!(cc.cong_mut(&app_5, &x));
    cc.clear();
    cc.merge(app_5.clone(), x.clone());
    assert!(!cc.cong_mut(&app_1, &x));
    assert!(!cc.cong_mut(&app_3, &x));
    assert!(cc.cong_mut(&app_5, &x));
    assert!(!cc.cong_mut(&app_5, &app_2));
    cc.merge(app_3.clone(), x.clone());
    assert!(cc.cong_mut(&app_1, &x));
    assert!(cc.cong_mut(&app_2, &x));
    assert!(cc.cong_mut(&app_3, &x));
    assert!(cc.cong_mut(&app_5, &x));
}

#[test]
fn simple_constant_fn() {
    let mut cc = SymbolTreeCongruence::default();
    let f = sym!("f");
    let g = sym!("g");
    let x = sym!("x");
    let y = sym!("y");
    let f_x = sym!((f, x));
    let f_y = sym!((f, y));
    let g_x = sym!((g, x));
    let g_y = sym!((g, y));
    assert!(!cc.cong_mut(&f, &g));
    assert!(!cc.cong_mut(&x, &y));
    assert!(cc.cong_mut(&f_x, &f_x));
    assert!(cc.cong_mut(&f_y, &f_y));
    assert!(!cc.cong_mut(&f_x, &f_y));
    assert!(!cc.cong_mut(&g_x, &g_y));
    assert!(!cc.cong_mut(&f_x, &g_x));
    assert!(!cc.cong_mut(&f_x, &g_y));
    assert!(!cc.cong_mut(&f_y, &g_x));
    assert!(!cc.cong_mut(&f_y, &g_y));
    cc.set_kind(f.clone(), FunctionKind::Constant).unwrap();
    assert!(!cc.cong_mut(&f, &g));
    assert!(!cc.cong_mut(&x, &y));
    assert!(cc.cong_mut(&f_x, &f_x));
    assert!(cc.cong_mut(&f_y, &f_y));
    assert!(cc.cong_mut(&f_x, &f_y));
    assert!(!cc.cong_mut(&g_x, &g_y));
    assert!(!cc.cong_mut(&f_x, &g_x));
    assert!(!cc.cong_mut(&f_x, &g_y));
    assert!(!cc.cong_mut(&f_y, &g_x));
    assert!(!cc.cong_mut(&f_y, &g_y));
}

#[test]
fn nested_constant_fn() {
    let mut cc = SymbolTreeCongruence::default();
    let f = sym!("f");
    let g = sym!("g");
    let h = sym!("h");
    let x = sym!("x");
    let y = sym!("y");
    let goal = sym!("goal");
    let f_x = sym!((f, x));
    let g_x = sym!((g, x));
    let fgfg = sym!((f, (g, (f, (g, x)))));
    let h_fgfg_fy = sym!((h, [(fgfg), (f, y)]));
    let h_f_f = sym!((h, [f_x, f_x]));
    let h_g_g = sym!((h, [g_x, g_x]));
    cc.merge(h_f_f.clone(), goal.clone());
    assert!(!cc.cong_mut(&h_fgfg_fy, &goal));
    cc.set_kind(f, FunctionKind::Constant).unwrap();
    //NOTE: constant function techniques may require insertion
    assert!(!cc.cong_mut(&h_fgfg_fy, &goal));
    assert!(!cc.cong_mut(&h_g_g, &goal));
    cc.insert(h_fgfg_fy.clone());
    assert!(cc.cong_mut(&h_fgfg_fy, &goal));
    assert!(!cc.cong_mut(&h_g_g, &goal));
}
