use congruence::{CongruenceClosure, DisjointSetForest, Equate, Pending, UnionFind, UseSet};
use hashbrown::{hash_map::Entry, HashMap};
use rand::{prelude::SliceRandom, Rng};
use std::hash::{BuildHasher, Hash};

/// A simple implementation of congruence closure, parametric over an arbitrary disjoint set forest implementation.
///
/// Based on Nieuwenhuis and Oliveras [Proof-producing Congruence Closure](https://www.cs.upc.edu/~oliveras/rta05.pdf).
#[derive(Debug, Clone)]
pub struct BinaryCongruenceClosure<I = usize, S = hashbrown::hash_map::DefaultHashBuilder> {
    /// The use-lists: for each representative `a`, a list of input equations `b_1(b_2) = b` where, for some `i`, `a ~ b_i`
    use_lists: HashMap<I, Vec<(I, I, I)>, S>,
    /// A lookup table mapping pairs of representatives `(b, c)` to input equations `a_1(a_2) = a` where `b ~ a_1` and `c ~ a_2` iff such an equation exists.
    /// We have the invariant that `use_lists[b].contains(a)` and `use_lists[c].contains(a)` iff `lookup[(b, c)] = "a_1(a_2) = a"`.
    lookup: HashMap<(I, I), (I, I, I), S>,
    /// The current list of pending unions
    pending: Vec<(I, I)>,
}

impl<I> BinaryCongruenceClosure<I> {
    fn new() -> Self {
        Self::default()
    }
}

impl<I, S: Default + BuildHasher> Default for BinaryCongruenceClosure<I, S> {
    fn default() -> Self {
        Self {
            use_lists: Default::default(),
            lookup: Default::default(),
            pending: Default::default(),
        }
    }
}

impl<I, S> BinaryCongruenceClosure<I, S>
where
    I: Hash + Copy + Eq,
    S: BuildHasher,
{
    /// Whether this congruence closure is empty, i.e. contains no *congruence* relations
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.use_lists.is_empty() && self.lookup.is_empty()
    }

    /// Register an equation of the form `left * right = result`
    pub fn equation(&mut self, left: I, right: I, result: I, union_find: &mut impl UnionFind<I>) {
        let left_repr = union_find.find_mut(left);
        let right_repr = union_find.find_mut(right);
        match self.lookup.entry((left_repr, right_repr)) {
            Entry::Occupied(fb) => {
                let b = fb.get().2;
                self.merge(result, b, union_find)
            }
            Entry::Vacant(v) => {
                v.insert((left, right, result));
                self.use_lists
                    .entry(left_repr)
                    .or_default()
                    .push((left, right, result));
                self.use_lists
                    .entry(right_repr)
                    .or_default()
                    .push((left, right, result));
            }
        }
    }

    /// Merge the equivalence classes of two nodes
    #[inline]
    pub fn merge(&mut self, mut a: I, mut b: I, union_find: &mut impl UnionFind<I>) {
        loop {
            let a_repr = union_find.find_mut(a);
            let b_repr = union_find.find_mut(b);
            if a_repr != b_repr {
                let new_repr = union_find.union_find(a, b);

                debug_assert!(union_find.find_mut(a) == new_repr);
                debug_assert!(union_find.find_mut(b) == new_repr);

                let old_repr = if new_repr == a_repr {
                    b_repr
                } else {
                    debug_assert!(new_repr == b_repr);
                    a_repr
                };
                debug_assert!(old_repr != new_repr);

                if let Some(list) = self.use_lists.remove(&old_repr) {
                    for (c_0, c_1, c) in list {
                        let left_repr = union_find.find_mut(c_0);
                        let right_repr = union_find.find_mut(c_1);
                        match self.lookup.entry((left_repr, right_repr)) {
                            Entry::Occupied(d) => {
                                self.pending.push((c, d.get().2));
                            }
                            Entry::Vacant(v) => {
                                v.insert((c_0, c_1, c));
                                self.use_lists
                                    .entry(new_repr)
                                    .or_default()
                                    .push((c_0, c_1, c));
                            }
                        }
                    }
                }
            }
            if let Some(pending) = self.pending.pop() {
                a = pending.0;
                b = pending.1;
            } else {
                return;
            }
        }
    }
}

#[test]
fn congruence_chain() {
    let mut dsf = DisjointSetForest::with_capacity(10);
    let mut cc = CongruenceClosure::new();
    let mut us = UseSet::new();
    let mut cs = Pending::new();
    let mut bin_dsf = DisjointSetForest::with_capacity(10);
    let mut bin_cc = BinaryCongruenceClosure::new();

    let (a, b, c, d, e, f, g, h) = (0, 1, 2, 3, 4, 5, 6, 7);

    // Set a * b ~ c
    cc.equation(Equate((a, b), c), &mut dsf, &mut us, &mut cs);
    bin_cc.equation(a, b, c, &mut bin_dsf);
    // Set a * d ~ e
    cc.equation(Equate((a, d), e), &mut dsf, &mut us, &mut cs);
    bin_cc.equation(a, d, e, &mut bin_dsf);
    // Set f * c ~ g
    cc.equation(Equate((f, c), g), &mut dsf, &mut us, &mut cs);
    bin_cc.equation(f, c, g, &mut bin_dsf);
    // Set f * e ~ h
    cc.equation(Equate((f, e), h), &mut dsf, &mut us, &mut cs);
    bin_cc.equation(f, e, h, &mut bin_dsf);

    assert!(dsf.is_empty());
    assert!(bin_dsf.is_empty());

    // If b ~ d, then a * b ~ c ~ a * d ~ e, and therefore f * c ~ g ~ f * e ~ h
    cc.merge(b, d, &mut dsf, &mut us, &mut cs);
    bin_cc.merge(b, d, &mut bin_dsf);
    assert!(!dsf.is_empty());
    assert!(dsf.node_eq_mut(b, d));
    assert!(dsf.node_eq_mut(c, e));
    assert!(dsf.node_eq_mut(g, h));
    assert!(!dsf.node_eq_mut(a, b));
    assert!(!dsf.node_eq_mut(a, c));
    assert!(!dsf.node_eq_mut(a, g));
    assert!(!dsf.node_eq_mut(b, c));
    assert!(!dsf.node_eq_mut(b, g));
    assert!(!dsf.node_eq_mut(c, g));
    assert_eq!(dsf, bin_dsf);
}

// Ignore in MIRI because too slow
#[test]
#[cfg_attr(miri, ignore)]
fn random_2048_256() {
    let mut rng = rand::thread_rng();
    let mut dsf = DisjointSetForest::with_capacity(3);
    let mut cc_dsf = DisjointSetForest::with_capacity(30);
    let mut cc_end_dsf = DisjointSetForest::with_capacity(64);
    let mut bin_cc_dsf = DisjointSetForest::with_capacity(30);
    let mut bin_cc_end_dsf = DisjointSetForest::with_capacity(64);
    let mut cc = CongruenceClosure::new();
    let mut cc_end = CongruenceClosure::new();
    let mut us = UseSet::new();
    let mut us_end = UseSet::new();
    let mut st = Pending::default();
    let mut bin_cc = BinaryCongruenceClosure::new();
    let mut bin_cc_end = BinaryCongruenceClosure::new();
    let mut equations = Vec::with_capacity(4096);

    for _ in 0..2048 {
        let a = rng.gen::<usize>() % 256;
        let b = rng.gen::<usize>() % 256;
        dsf.union(a, b);
        cc_end.merge(a, b, &mut cc_end_dsf, &mut us_end, &mut st);
        cc.merge(a, b, &mut cc_dsf, &mut us, &mut st);
        bin_cc_end.merge(a, b, &mut bin_cc_end_dsf);
        bin_cc.merge(a, b, &mut bin_cc_dsf);
        let c = rng.gen::<usize>() % 256;
        let d = rng.gen::<usize>() % 256;
        dsf.union(c, d);
        cc_end.merge(c, d, &mut cc_end_dsf, &mut us_end, &mut st);
        cc.merge(c, d, &mut cc_dsf, &mut us, &mut st);
        bin_cc_end.merge(c, d, &mut bin_cc_end_dsf);
        bin_cc.merge(c, d, &mut bin_cc_dsf);
        let e = rng.gen::<usize>() % 256;
        let f = rng.gen::<usize>() % 256;
        cc.equation(Equate((a, b), c), &mut cc_dsf, &mut us, &mut st);
        cc.equation(Equate((d, e), f), &mut cc_dsf, &mut us, &mut st);
        bin_cc.equation(a, b, c, &mut bin_cc_dsf);
        bin_cc.equation(d, e, f, &mut bin_cc_dsf);
        equations.push((a, b, c, d, e, f));
        let e = e + 256;
        let f = f + 256;
        cc.equation(Equate((a, b), c), &mut cc_dsf, &mut us, &mut st);
        cc.equation(Equate((d, e), f), &mut cc_dsf, &mut us, &mut st);
        bin_cc.equation(a, b, c, &mut bin_cc_dsf);
        bin_cc.equation(d, e, f, &mut bin_cc_dsf);
        equations.push((a, b, c, d, e, f));
    }
    assert!(dsf.refines_mut(&mut cc_dsf));
    assert!(!cc_dsf.refines_mut(&mut dsf));
    assert_eq!(dsf, cc_end_dsf);
    assert_eq!(cc_dsf, bin_cc_dsf);
    assert_eq!(cc_end_dsf, bin_cc_end_dsf);

    equations.shuffle(&mut rng);
    for (a, b, c, d, e, f) in equations {
        cc_end.equation(Equate((a, b), c), &mut cc_end_dsf, &mut us_end, &mut st);
        cc_end.equation(Equate((d, e), f), &mut cc_end_dsf, &mut us_end, &mut st);
        bin_cc_end.equation(a, b, c, &mut bin_cc_end_dsf);
        bin_cc_end.equation(d, e, f, &mut bin_cc_end_dsf);
        assert!(dsf.refines_mut(&mut cc_end_dsf));
        assert!(cc_end_dsf.refines_mut(&mut cc_dsf));
    }
    assert_ne!(dsf, cc_end_dsf);
    assert_eq!(cc_dsf, cc_end_dsf);
    assert_ne!(dsf, cc_end_dsf);
    assert_eq!(cc_dsf, bin_cc_dsf);
    assert_eq!(cc_end_dsf, bin_cc_end_dsf);
}
