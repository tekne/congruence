use std::hash::{Hash, Hasher};

use congruence::{
    CongruenceClosure, ConstLanguage, DisjointSetForest, EqMod, Equate, HashMod, Language, Pending,
    UnionFind, UseSet,
};

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct NilHash(usize);

impl Hash for NilHash {
    fn hash<H: Hasher>(&self, state: &mut H) {
        0.hash(state)
    }
}

impl<U: UnionFind<usize>> ConstLanguage<U, usize> for NilHash {}

impl<U: UnionFind<usize>> Language<U, usize> for NilHash {
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&U, usize) -> Result<(), E>,
        ctx: &U,
    ) -> Result<(), E> {
        visitor(ctx, self.0)
    }

    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut U, usize) -> Result<(), E>,
        ctx: &mut U,
    ) -> Result<(), E> {
        visitor(ctx, self.0)
    }
}

impl<U: UnionFind<usize>> EqMod<U, usize> for NilHash {
    fn eq_mod(
        &self,
        other: &Self,
        eq_mod: &mut impl FnMut(&U, usize, usize) -> bool,
        ctx: &U,
    ) -> bool {
        eq_mod(ctx, self.0, other.0)
    }

    fn eq_mod_mut(
        &self,
        other: &Self,
        eq_mod: &mut impl FnMut(&mut U, usize, usize) -> bool,
        ctx: &mut U,
    ) -> bool {
        eq_mod(ctx, self.0, other.0)
    }
}

impl<U: UnionFind<usize>> HashMod<U, usize> for NilHash {
    fn hash_mod<H: Hasher>(
        &self,
        hasher: &mut H,
        _hash_mod: &mut impl FnMut(&mut H, &U, usize),
        _ctx: &U,
    ) {
        0.hash(hasher);
    }

    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        _hash_mod: &mut impl FnMut(&mut H, &mut U, usize),
        _ctx: &mut U,
    ) {
        0.hash(hasher);
    }
}

#[test]
fn congruence_chain() {
    let mut dsf = DisjointSetForest::with_capacity(10);
    let mut cc = CongruenceClosure::new();
    let mut us = UseSet::new();
    let mut cs = Pending::new();
    let mut dsf_nil = DisjointSetForest::with_capacity(10);
    let mut cc_nil = CongruenceClosure::new();
    let mut us_nil = UseSet::new();
    let mut cs_nil = Pending::new();

    let (a, b, c, d, e, f, g, h) = (0, 1, 2, 3, 4, 5, 6, 7);

    // Set a * b ~ c
    cc.equation(Equate((a, b), c), &mut dsf, &mut us, &mut cs);
    cc_nil.equation(
        Equate((NilHash(a), NilHash(b)), c),
        &mut dsf_nil,
        &mut us_nil,
        &mut cs_nil,
    );

    // Set a * d ~ e
    cc.equation(Equate((a, d), e), &mut dsf, &mut us, &mut cs);
    cc_nil.equation(
        Equate((NilHash(a), NilHash(d)), e),
        &mut dsf_nil,
        &mut us_nil,
        &mut cs_nil,
    );


    // Set f * c ~ g
    cc.equation(Equate((f, c), g), &mut dsf, &mut us, &mut cs);
    cc_nil.equation(
        Equate((NilHash(f), NilHash(c)), g),
        &mut dsf_nil,
        &mut us_nil,
        &mut cs_nil,
    );

    // Set f * e ~ h
    cc.equation(Equate((f, e), h), &mut dsf, &mut us, &mut cs);
    cc_nil.equation(
        Equate((NilHash(f), NilHash(e)), h),
        &mut dsf_nil,
        &mut us_nil,
        &mut cs_nil,
    );

    assert!(dsf.is_empty());
    assert!(dsf_nil.is_empty());

    // If b ~ d, then a * b ~ c ~ a * d ~ e, and therefore f * c ~ g ~ f * e ~ h
    cc.merge(b, d, &mut dsf, &mut us, &mut cs);
    cc_nil.merge(b, d, &mut dsf_nil, &mut us_nil, &mut cs_nil);
    assert!(!dsf.is_empty());
    assert!(dsf.node_eq_mut(b, d));
    assert!(dsf.node_eq_mut(c, e));
    assert!(dsf.node_eq_mut(g, h));
    assert!(!dsf.node_eq_mut(a, b));
    assert!(!dsf.node_eq_mut(a, c));
    assert!(!dsf.node_eq_mut(a, g));
    assert!(!dsf.node_eq_mut(b, c));
    assert!(!dsf.node_eq_mut(b, g));
    assert!(!dsf.node_eq_mut(c, g));
    assert_eq!(dsf, dsf_nil);
}
