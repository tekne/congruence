# 0.4.0

- Separated `UseSet` from `CongruenceClosure`; the former should now be passed around
- Split the `Language` trait into `EqMod`, `HashMod`, and a new `Language` trait
- Added the `ConstLanguage` trait
- Added the `Equation` trait, re-implemented `CongruenceClosure` to be based on the `Equation` trait rather than `(Language, Index)` pairs
- Improved symbol lang example to support known-constant functions
- Renamed `CongruenceState` to `Pending`, increased access to internals in public API
- Added `_pending` family of API functions to `CongruenceClosure`

# 0.3.0

- Generalized `Language` trait so that `hash_mod` takes a closure receiving a `Hasher`

# 0.2.0

- Generalized `CongruenceClosure` to work over any type `L` of expressions satisfying the new `Language` trait
- Renamed `CongruenceClosure::union` to `CongruenceClosure::merge`
- Added utility functions to `CongruenceClosure`, e.g. `lookup` and `expr_eq`
- Improved memory usage of `CongruenceClosure`

# 0.1.1

- Implemented `Clone` for `CongruenceState`
- Made `CongruenceState = CongruenceState<usize>` by default 

# 0.1.0

Initial release
- Added `CongruenceClosure`, `CongruenceState`, and `DisjointSetForest` data structures
- Added `UnionFind` trait
